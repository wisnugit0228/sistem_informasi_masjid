@extends('layouts.main')

@section('content')
    

<form action="/berita/{{ $berita->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group col-xl-6 col-lg-8">
        <div class="img-content col-sm-6">
            <img src="/fotoberita/{{ $berita->foto }}" class="img-fluid" alt="...">
        </div>
        <label for="foto" class="form-label">Foto</label>
        <input type="file" class="form-control" id="foto" name="foto"  >
    </div>

      <div class="form-group col-xl-6 col-lg-8">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" name="judul" id="judul" value="{{ $berita->judul }}">
      </div>
      @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      <div class="form-group col-xl-6 col-lg-8">
          <label for="konten">Konten</label>
          <textarea class="form-control" id="konten" name="konten" rows="3">{{ $berita->konten }}</textarea>
          
      </div>
      @error('konten')
      <div class="alert alert-danger">{{ $message }}</div>
  
      @enderror

     <input type="text" name="masjid_id" value="{{ $berita->masjid_id }}">
     
      <br>
      <button type="submit" class="btn btn-primary">Submit</button>
      
      
</form>


@endsection