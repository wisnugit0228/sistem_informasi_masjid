<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <br><br><br>
    <div class="container">
        <center>
           
            @foreach ($masuk as $item)
                @if ($item->bulan === '2023-12')
                    @php
                        $bulan = 'Desember';
                    @endphp
                @endif
                @if ($item->bulan === '2023-11')
                    @php
                        $bulan = 'November';
                    @endphp
                @endif
                @if ($item->bulan === '2023-10')
                    @php
                        $bulan = 'Oktober';
                    @endphp
                @endif
            @endforeach
           
            <h3>Laporan Uang Masuk Bulan @php
                                    echo $bulan;
                                @endphp 
            </h3>
            <table class="table table-bordered table-striped">
                <thead style="background-color: lightskyblue">
                  <tr>
                    <th scope="col" style="text-align: center">Tanggal</th>
                    <th scope="col" style="text-align: center">Uang Masuk</th>
                    <th scope="col" style="text-align: center">Rincian</th>
                  </tr>
                </thead>
                <tbody>
                    @php
                        $total = 0;
                    @endphp
                  @forelse ($masuk as $item)
                    @php
                        $total += $item->uangmasuk;
                    @endphp
                 
                  <tr>
                    <td>{{ $item->tgl }}</td>
                    <td>@currency($item->uangmasuk)</td>
                    <td>{{ $item->keterangan }}</td>

                  </tr>
                  @empty
                      
                  @endforelse
                  <td colspan="3" style="text-align: center"><b>Total Uang Masuk : @currency($total)</b></td>
                 
                 
                </tbody>
              </table>
    
        </center>
        
    </div>

    <script type="text/javascript">
        window.print();
    </script>
    
</body>
</html>

