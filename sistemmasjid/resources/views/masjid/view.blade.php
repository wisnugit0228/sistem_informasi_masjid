@extends('layouts.main')

@section('content')
@if ($mypost=$mypost)

    <div class="container">
      @if ($mypost1=$mypost1)

      <div class="card bg-success">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6">
              <b>Jadwal Sholat</b>
              <h6><b>Di Kecamatan Rambang Niru, {{ $mytime->format('D M Y') }}</b></h6>

            </div>
           
           <div class="kotak mr-2">
            <center>
              <b style="color: black">Subuh</b>
              <p style="color: black">{{ $sholat->subuh }}</p>
            </center>
           </div>

           <div class="kotak mr-2">
            <center>
              <b style="color: black">Dzuhur</b>
              <p style="color: black">{{ $sholat->dzuhur }}</p>
            </center>
           </div>


           <div class="kotak mr-2">
            <center>
              <b style="color: black">Ashar</b>
              <p style="color: black">{{ $sholat->ashar }}</p>
            </center>
           </div>


           <div class="kotak mr-2">
            <center>
              <b style="color: black">Magrib</b>
              <p style="color: black">{{ $sholat->magrib }}</p>
            </center>
           </div>


           <div class="kotak mr-2">
            <center>
              <b style="color: black">Isya</b>
              <p style="color: black">{{ $sholat->isya }}</p>
            </center>
           </div>
           
            <div class="col-lg-1">
              <a href="sholat/{{ $sholat->id }}/edit" class="btn btn-warning">Ubah</a>
            </div>
          </div>
         
        </div>
      </div>
      
          
      @else
      <div class="card bg-success">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6">
              <b>Jadwal Sholat</b>
              <h6><b>Di Kecamatan Rambang Niru, Minggu, 3 Desember 2023</b></h6>

            </div>
           
           <div class="kotak mr-2">
            <center>
              <b style="color: black">Subuh</b>
              <p style="color: black">00.00</p>
            </center>
           </div>

           <div class="kotak mr-2">
            <center>
              <b style="color: black">Dzuhur</b>
              <p style="color: black">00.00</p>
            </center>
           </div>


           <div class="kotak mr-2">
            <center>
              <b style="color: black">Ashar</b>
              <p style="color: black">00.00</p>
            </center>
           </div>


           <div class="kotak mr-2">
            <center>
              <b style="color: black">Magrib</b>
              <p style="color: black">00.00</p>
            </center>
           </div>


           <div class="kotak mr-2">
            <center>
              <b style="color: black">Isya</b>
              <p style="color: black">00.00</p>
            </center>
           </div>
            
          </div>
        </div>
      </div>
      <a href="sholat/create" class="btn btn-warning">Tambah</a>
      
          
      @endif
        <center>
            <h1>
                Selamat Datang Admin {{ $masjid->nama }}
            </h1>
        </center>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="/fotomasjid/{{ $masjid->foto }}" class="d-block w-100" alt="...">
            </div>
            @foreach ($foto as $item)
            <div class="carousel-item">
              <img src="/galeri/{{ $item->foto }}" class="d-block w-100" alt="...">
            </div>
            @endforeach
            
          </div>
         <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </button>
          <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </button>
        </div>
       
      
        <div>
            <h5>Alamat:</h5>
            <p>{{ $masjid->alamat }}</p>
            <h5>Tentang:</h5>
            <p>{{ $masjid->tentang }}</p>
            <div class="container">
              <a href="masjid/{{ $masjid->id }}/edit" class="btn btn-warning">Ubah</a>
          </div>
        </div>
        <br>

       

        
    </div>

    
    
 @else

<a href="masjid/create" class="btn btn-primary">Tambah</a>

    
@endif 


@endsection