<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masjid extends Model
{
    use HasFactory;
    protected $table = 'masjid';
    protected $fillable = ['nama','foto', 'alamat', 'tentang', 'profile_id'];


    public function profile(){
        return $this->belongsTo(Profile::class, 'profile_id');
    }

    public function anggota(){
        return $this->hasMany(Anggota::class, 'masjid_id');
    }

    public function masuk(){
        return $this->hasMany(Masuk::class, 'masjid_id');
    }

    public function gambar(){
        return $this->hasMany(Gambar::class, 'masjid_id');
    }

    public function berita(){
        return $this->hasMany(Berita::class, 'masjid_id');
    }
}
