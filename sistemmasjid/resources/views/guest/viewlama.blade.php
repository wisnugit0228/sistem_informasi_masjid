@extends('layouts.main')

@section('content')

@foreach ($masjid as $item)

<div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-3">
          <img src="/fotomasjid/{{ $item->foto }}" class="img-fluid" alt="...">
        </div>

        <div class="col-9">
          <h3>{{ $item->nama }}</h3>
          <p class="berita">{{ $item->tentang }}</p>
          <a href="/masjid/{{ $item->id }}" class="btn btn-primary">Selengkapnya</a>
            
          
        </div>
        
      </div>
    </div>
  </div>
    
@endforeach

@endsection