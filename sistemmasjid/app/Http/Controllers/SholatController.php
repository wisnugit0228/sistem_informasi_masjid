<?php

namespace App\Http\Controllers;

use App\Models\Sholat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SholatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        
        $mypost = Sholat::where('masjid_id', $idmasjid)->first();
        $sholat = Sholat::get()->where('masjid_id', $idmasjid);
        

        return view('jadwalsholat.view', ['sholat'=>$sholat], ['mypost'=>$mypost]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id; 
        return view ('jadwalsholat.tambah', ['masjid'=>$masjid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'subuh' => 'required',
            'dzuhur'=> 'required',
            'ashar'=> 'required',
            'magrib'=> 'required',
            'isya'=> 'required',
            'masjid_id'=> 'required',
            
        ]);


        $jadwal = new Sholat;
        $jadwal->subuh = $request->subuh;
        $jadwal->dzuhur = $request->dzuhur;
        $jadwal->ashar = $request->ashar;
        $jadwal->magrib = $request->magrib;
        $jadwal->isya = $request->isya;
        $jadwal->masjid_id = $request->masjid_id;

        $jadwal->save();

        return redirect('masjid');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sholat  $sholat
     * @return \Illuminate\Http\Response
     */
    public function show(Sholat $sholat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sholat  $sholat
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sholat = Sholat::get()->where('id', $id)->first();
        return view('jadwalsholat.ubah', ['sholat'=>$sholat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sholat  $sholat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'subuh' => 'required',
            'dzuhur' => 'required',
            'ashar' => 'required',
            'magrib' => 'required',
            'isya' => 'required',
            'masjid_id'=> 'required'
        ]);

        $berita = Sholat::find($id);

        $berita->subuh = $request['subuh'];
        $berita->dzuhur = $request['dzuhur'];
        $berita->ashar = $request['ashar'];
        $berita->magrib = $request['magrib'];
        $berita->isya = $request['isya'];
        
        $berita->masjid_id = $request['masjid_id'];
        $berita->save();

        return redirect('masjid');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sholat  $sholat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sholat $sholat)
    {
        //
    }
}
