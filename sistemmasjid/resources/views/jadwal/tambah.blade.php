@extends('layouts.main')

@section('content')

<form action="/jadwal" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="form-group col-xl-6 col-lg-8">
        <label for="tgl" class="form-label">Tanggal</label>
        <input type="date" class="form-control" id="tgl" name="tgl"  min="{{ $mytime->format('Y-m-d') }}">
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="imam" class="form-label">Imam</label>
       
        <input type="text" name="imam" list="imam" class="form-control">
        <datalist id="imam">
            @foreach ($i as $item)
            @if ($item->masjid_id === $masjid)
            <option value="{{ $item->nama }}">
            @else
                
            @endif
            
            @endforeach
            
        </datalist>
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="khotib" class="form-label">Khotib</label>
        <input type="text" name="khotib" list="khotib" class="form-control">
        <datalist id="khotib">
            @foreach ($kh as $item)
            @if ($item->masjid_id === $masjid)
            <option value="{{ $item->nama }}">
            @else
                
            @endif
            
            @endforeach
            
        </datalist>
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="muadzin" class="form-label">Muadzin</label>
        <input type="text" name="muadzin" list="muadzin" class="form-control">
        <datalist id="muadzin">
            @foreach ($m as $item)
            @if ($item->masjid_id === $masjid)
            <option value="{{ $item->nama }}">
            @else
                
            @endif
            
            @endforeach
            
        </datalist>
    </div>

      <div class="form-group col-xl-6 col-lg-8">
          <label for="bulan">Bulan</label>
          <input type="month" class="form-control" name="bulan" id="bulan" min="{{ $mytime->format('Y-m') }}"  placeholder="Masukan Nama">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror



     

    <input type="text" name="masjid_id" value="{{ $masjid }}" style="display: none">

      
      <button type="submit" class="btn btn-primary">Submit</button>
      
      
</form>
    
@endsection