@extends('layouts.main')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <h3>Foto Anggota</h3>
                <img src="/fotoanggota/{{ $anggota->foto }}" class="img-thumbnail" alt="...">
            </div>
            <div class="col-8 mt-5">
                <h5>Nama:</h5>
                <p>{{ $anggota->nama }}</p>
                <h5>Status:</h5>
                <p>{{ $anggota->status }}</p>
                <h5>Alamat</h5>
                <p>{{ $anggota->alamat }}</p>
            </div>

        </div>
        
    </div>
  </div>
    
@endsection