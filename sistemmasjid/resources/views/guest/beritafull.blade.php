@extends('layouts.main3')


@section('nav')
<div class="container">
    {{-- <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="..." /></a> --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ms-1"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item"><a class="nav-link" href="/masjid/{{ $idmasjid->masjid_id }}">Beranda</a></li>
         
        </ul>
    </div>
</div>
@endsection


@section('content')
    
    <hr>
    <center>
        <h3>Berita Masjid</h3>
        
    </center>
    <hr>
    <div class="row">
        @foreach ($berita as $item)

        <div class="col-lg-3">
            <div class="card">
                <div style="max-height: 150px; overflow:hidden;">
                  <img src="/fotoberita/{{ $item->foto }}"  class="card-img-top" alt="...">
                </div>
                
                <div class="card-body">
                  <h5 class="card-title"><b>{{ $item->judul }}</b></h5>
                  <p class="foto card-text"><span>...</span></p>
                  <a href="/berita/{{ $item->id }}" class="btn btn-primary">Lihat</a>
                </div>
              </div>

        </div>

        

        @endforeach

      </div>

@endsection