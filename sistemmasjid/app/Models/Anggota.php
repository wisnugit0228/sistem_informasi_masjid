<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    use HasFactory;
    protected $table = 'anggota';
    protected $fillable = ['nama','foto', 'alamat', 'status', 'masjid_id'];

    public function masjid(){
        return $this->belongsTo(Masjid::class, 'masjid_id');
    }
}
