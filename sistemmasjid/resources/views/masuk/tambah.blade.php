@extends('layouts.main')


@section('content')
    <form action="/saldomasuk" method="POST" enctype="multipart/form-data">
        @csrf
    
    
          <div class="form-group col-xl-6 col-lg-8">
              <label for="uangmasuk">Uang Masuk</label>
              <input type="number" class="form-control" name="uangmasuk" id="uangmasuk"  placeholder="Masukan uangmasuk">
          </div>
          @error('uangmasuk')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror


          
          
          <div class="form-group col-xl-6 col-lg-8">
              <label for="keterangan">Keterangan</label>
              <textarea class="form-control" id="keterangan" name="keterangan" rows="3"></textarea>
              
          </div>
          @error('keterangan')
          <div class="alert alert-danger">{{ $message }}</div>
      
          @enderror

          <div class="form-group col-xl-6 col-lg-8">
            <label for="tgl">Tanggal</label>
            <input type="date" class="form-control" name="tgl" id="tgl"  >
            
        </div>
        @error('tgl')
        <div class="alert alert-danger">{{ $message }}</div>
    
        @enderror

          <div class="form-group col-xl-6 col-lg-8">
            <label for="minggu" class="form-label">Minggu Ke-</label>
            <select name="minggu" id="minggu" class="form-control" >
                <option value="">----</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                      
            </select>
        </div>
    
          <div class="form-group col-xl-6 col-lg-8">
              <label for="bulan">Bulan</label>
              <input type="month" class="form-control" name="bulan" id="bulan"  placeholder="Masukan Nama">
          </div>
          @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
    



        <input type="text" name="masjid_id" value="{{ $masjid }}" style="display: none">
    
          
          <button type="submit" class="btn btn-primary">Submit</button>
          
          
    </form>
@endsection