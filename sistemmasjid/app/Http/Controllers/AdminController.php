<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminController extends Controller
{
    public function galeri()
    {
        $masjid = Auth::user()->profile->masjid->id;
    
        return view ('gambar.tambahgaleri', ['masjid'=>$masjid]);
    }

    public function super()
    {
        return view('super');
    }

    public function dataadmin()
    {
        $data = User::get();
        return view('super.view', ['data'=>$data]);
    }
}
