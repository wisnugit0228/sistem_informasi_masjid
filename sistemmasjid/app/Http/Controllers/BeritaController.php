<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use App\Models\Masjid;
use Illuminate\Support\Facades\Auth;
use File;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        
        $mypost = Berita::where('masjid_id', $idmasjid)->first();
        $berita = Berita::get()->where('masjid_id', $idmasjid);
        

        return view('berita.view', ['berita'=>$berita], ['mypost'=>$mypost]);
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id; 
        return view ('berita.tambah', ['masjid'=>$masjid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg',
            'judul' => 'required',
            'konten'=> 'required',
            'masjid_id'=> 'required'
            
        ]);


        $fileName = time().'.'.$request->foto->extension();
        $request->foto->move(public_path('fotoberita'), $fileName);

        $masjid = new Berita;
        $masjid->foto = $fileName;
        $masjid->judul = $request->judul;
        $masjid->konten = $request->konten;
        $masjid->masjid_id = $request->masjid_id;

        $masjid->save();

        return redirect('berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::get()->where('id', $id)->first();
        return view('berita.detail', ['berita'=>$berita]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita = Berita::find($id);

        return view ('berita.edit', ['berita'=>$berita]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'foto' => 'image|mimes:jpg,png,jpeg',
            'konten' => 'required',
            'masjid_id'=> 'required'
        ]);

        $berita = Berita::find($id);
        if ($request->has('foto')) {
            $patch = 'fotoberita/';
            File::delete($patch.$berita->foto);

            $fileName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('fotoberita'), $fileName);

            $berita->foto = $fileName;


            $berita->save();

        }

        $berita->judul = $request['judul'];
        $berita->konten = $request['konten'];
        $berita->masjid_id = $request['masjid_id'];
        $berita->save();

        return redirect('berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);
        $patch = 'fotoberita/';
        File::delete($patch.$berita->foto);

        $berita->delete();

        return redirect('berita');
    }
}
