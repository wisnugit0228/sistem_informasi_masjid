<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluar extends Model
{
    use HasFactory;
    protected $table = 'keluar';

    protected $fillable = ['uangkeluar','keterangan', 'tgl', 'minggu', 'bulan', 'masjid_id'];

    public function masjid(){
        return $this->belongsTo(Masjid::class, 'masjid_id');
    }
}
