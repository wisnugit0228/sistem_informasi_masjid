@extends('layouts.main')

@section('content')
<div class="container">
    @if ($mypost=$mypost)
    <a href="/saldokeluar/create" class="btn btn-info">Tambah Data</a>

    <a href="/cetaksaldokeluar" class="btn btn-warning">Cetak</a>
    
    <br><br>

    {{-- Bulan Oktober --}}
   
          <h3 class="container mt-3">Laporan Uang Masuk Tahun 2023</h3>
      <table class="table table-bordered table-striped">
        <thead style="background-color: lightskyblue">
          <tr>
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($keluar as $item)
          <tr>
            <td>{{ $item->tgl }}</td>
            <td>{{ $item->uangkeluar }}</td>
            <td>{{ $item->keterangan }}</td>
          </tr>
          @empty
              
          @endforelse
         
         
        </tbody>
      </table>
         
        </tbody>
      </table>
      
    @else
        <a href="/saldokeluar/create" class="btn btn-info">Tambah Data</a>
    @endif
   
</div>
@endsection