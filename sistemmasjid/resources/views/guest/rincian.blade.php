@extends('layouts.main')

@section('content')
<div class="container">
    
   

    {{-- Bulan Oktober --}}
    <h3 class="container mt-3">Laporan Bulan Oktober</h3>
    <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Minggu Ke-</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
          {{-- minggu pertama --}}
          @php
            $total = 0;
          @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-10' and $item->minggu === '1')
            <tr>
               <td>1</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
                $total += $item->uangmasuk;
              @endphp
            
            </tr>
                
            @else
            @endif
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-1 : @currency($total)</b></p></td>
            </tr>

            {{-- minggu kedua --}}
            @php
              $total2 = 0;
            @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-10' and $item->minggu === '2')
            <tr>
               <td>2</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
              $total2 += $item->uangmasuk;
              @endphp
            </tr>
                
            @else
                
            @endif
               
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-2 : @currency($total2)</b></p></td>
            </tr>


            {{-- minggu ketiga --}}

            @php
              $total3 = 0;
            @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-10' and $item->minggu === '3')
            <tr>
               <td>3</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
              $total3 += $item->uangmasuk;
              @endphp
            </tr>
                
            @else
                
            @endif
               
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-3 : @currency($total3)</b></p></td>
            </tr>

            {{-- minggu keempat --}}
            @php
              $total4 = 0;
            @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-10' and $item->minggu === '4')
            <tr>
               <td>4</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
              $total4 += $item->uangmasuk;
              @endphp
            </tr>
                
            @else
                
            @endif
               
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-4 : @currency($total4)</b></p></td>
            </tr>
         
        </tbody>
      </table>
      {{-- End Bulan Oktober --}}

      {{-- Bulan November --}}

      <h3 class="container mt-3">Laporan Bulan November</h3>
    <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Minggu Ke-</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
          {{-- minggu pertama --}}
          @php
            $totalnov1 = 0;
          @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-11' and $item->minggu === '1')
            <tr>
               <td>1</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
                $totalnov1 += $item->uangmasuk;
              @endphp
            
            </tr>
                
            @else
            @endif
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-1 : @currency($totalnov1)</b></p></td>
            </tr>

            {{-- minggu kedua --}}
            @php
              $totalnov2 = 0;
            @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-11' and $item->minggu === '2')
            <tr>
               <td>2</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
              $totalnov2 += $item->uangmasuk;
              @endphp
            </tr>
                
            @else
                
            @endif
               
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-2 : @currency($totalnov2)</b></p></td>
            </tr>


            {{-- minggu ketiga --}}

            @php
              $totalnov3 = 0;
            @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-11' and $item->minggu === '3')
            <tr>
               <td>3</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
              $totalnov3 += $item->uangmasuk;
              @endphp
            </tr>
                
            @else
                
            @endif
               
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-3 : @currency($totalnov3)</b></p></td>
            </tr>

            {{-- minggu keempat --}}
            @php
              $totalnov4 = 0;
            @endphp
            @foreach ($masuk as $item)

            @if ($item->bulan === '2023-11' and $item->minggu === '4')
            <tr>
               <td>4</td>
              <td>{{ $item->tgl}} </td>
              <td>@currency($item->uangmasuk)</td>
              <td>{{ $item->keterangan }}</td>
              @php
              $totalnov4 += $item->uangmasuk;
              @endphp
            </tr>
                
            @else
                
            @endif
               
            @endforeach
            <tr>
              <td colspan="4"><p><b> Total Pemasukan Minggu Ke-4 : @currency($totalnov4)</b></p></td>
            </tr>
         
        </tbody>
      </table>



      {{-- <h3 class="container mt-3">Laporan Bulan November</h3>
    <table class="table">
        <thead>
          <tr>
            
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Uang Keluar</th>
            <th scope="col">Saldo</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
           
            @foreach ($keuangan as $item)

            @if ($item->bulan === '2023-11')
            <tr>
               
              <td>Jumat, {{ $item->tgl }}</td>
              <td>{{ $item->masuk }}</td>
              <td>{{ $item->keluar }}</td>
              <td>{{ $item->saldo }}</td>
              <td>
                      <a href="keuangan/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a>   
                    
              </td>
            </tr>
                
            @else
                
            @endif

            
               
            @endforeach
         
        </tbody>
      </table>

      <h3 class="container mt-3">Laporan Bulan Desember</h3>
    <table class="table">
        <thead>
          <tr>
            
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Uang Keluar</th>
            <th scope="col">Saldo</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
           
            @foreach ($keuangan as $item)

            @if ($item->bulan === '2023-12')
            <tr>
               
              <td>Jumat, {{ $item->tgl }}</td>
              <td>{{ $item->masuk }}</td>
              <td>{{ $item->keluar }}</td>
              <td>{{ $item->saldo }}</td>
              <td>
                      <a href="keuangan/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a>   
                    
              </td>
            </tr>
                
            @else
                
            @endif

          
               
            @endforeach
         
        </tbody>
      </table> --}}
   
   
</div>
@endsection