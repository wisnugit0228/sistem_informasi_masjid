<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keuangan', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('tgl');
            $table->integer('masuk');
            $table->integer('keluar');
            $table->integer('saldo');
            $table->string('rincian');
            $table->string('bulan');
            $table->unsignedBigInteger('masjid_id');
            $table->foreign('masjid_id')->references('id')->on('masjid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keuangan');
    }
}
