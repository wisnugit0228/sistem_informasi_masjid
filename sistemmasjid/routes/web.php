<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\GambarController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\MasjidController;
use App\Http\Controllers\SholatController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KeuanganController;
use App\Http\Controllers\SaldomasukController;
use App\Http\Controllers\TotalmasukController;
use App\Http\Controllers\SaldokeluarController;
use App\Http\Controllers\CetakController;
use App\Http\Controllers\SaranController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/dev', function () {
//     return view('welcome');
// });

Route::get('/', [GuestController::class, 'index']);

Route::get('/detailmasjid/{$id}', [GuestController::class, 'lihat']);
Route::get('/rinciansaldomasuk/{id}/rincian', [GuestController::class, 'rinciansaldo']);
Route::get('/rinciansaldokeluar/{id}/rincian', [GuestController::class, 'rinciansaldokeluar']);
Route::get('/jadwaljumat/{id}/jadwal', [GuestController::class, 'jadwal']);
Route::get('/daftarpetugas/{id}/daftar', [GuestController::class, 'petugas']);
Route::get('/laporankeuangan/{id}/daftar', [GuestController::class, 'laporan']);
Route::get('/fullgaleri/{id}/daftar', [GuestController::class, 'galerifull']);
Route::get('/fullberita/{id}/daftar', [GuestController::class, 'beritafull']);

Route::get('/cetaksaldomasuk', [CetakController::class, 'cetakmasuk']);

Route::get('/cetaksaldomasuk/cari', [CetakController::class, 'carimasuk']);

Route::get('/cetaksaldokeluar', [CetakController::class, 'cetakkeluar']);

Route::get('/cetaksaldokeluar/cari', [CetakController::class, 'carikeluar']);


Route::get('/saldomasuk/cari', [CetakController::class, 'caridatamasuk']);

Route::get('/saldokeluar/cari', [CetakController::class, 'caridatakeluar']);


Route::get('/tampil', [TestController::class, 'tampil']);

Route::get('/galeri', [AdminController::class, 'galeri']);

Route::get('/dataadmin', [AdminController::class, 'dataadmin']);

Route::get('/superadmin', [AdminController::class, 'super']);

Route::resource('saran', saranController::class);

Route::resource('sholat', SholatController::class);

Route::resource('profile', ProfileController::class);

Route::resource('gambar', GambarController::class);

Route::resource('masjid', MasjidController::class);

Route::resource('anggota', AnggotaController::class);

Route::resource('jadwal', JadwalController::class);

Route::resource('keuangan', KeuanganController::class);

Route::resource('saldomasuk', SaldomasukController::class);

Route::resource('saldokeluar', SaldokeluarController::class);

Route::resource('totalsaldomasuk', TotalmasukController::class);

Route::resource('berita', BeritaController::class);

Route::middleware(['auth'])->group(function () {

    Route::get('/register', function () {
        return view('welcome');
    });
    
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
