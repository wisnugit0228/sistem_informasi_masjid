@extends('layouts.main')

@section('content')
<div class="container">
    @if ($mypost=$mypost)
    <a href="/jadwal/create" class="btn btn-info">Tambah Data</a>
    
    <br><br>

    {{-- Bulan Oktober --}}
   
          <h3 class="container mt-3">Jadwal Petugas Sholat Jumat Bulan Ini</h3>
      <table class="table table-bordered table-striped">
        <thead style="background-color: lightskyblue">
          <tr>
            <th scope="col">Tanggal</th>
            <th scope="col">Imam</th>
            <th scope="col">Khotib</th>
            <th scope="col">Muadzin</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($jadwal as $item)
          @if ($item->bulan === $mytime->format('Y-m'))
          <tr>
            <td>{{ $item->tgl }}</td>
            <td>{{ $item->imam }}</td>
            <td>{{ $item->khotib }}</td>
            <td>{{ $item->muadzin }}</td>
          </tr>
          @else
              
          @endif
         
          @empty
              
          @endforelse
         
         
        </tbody>
      </table>
         
        </tbody>
      </table>
      
    @else
        <a href="/jadwal/create" class="btn btn-info">Tambah Data</a>
    @endif
   
</div>
@endsection