@extends('layouts.main')

@section('content')
<div class="container">
    @if ($mypost=$mypost and $mypostt=$mypostt)


    <div class="card">
      <div class="card-header" style="background-color: lightgray">
        <center>
          <h4>Total Uang Masuk dan Keluar Bulan Ini</h4>
        </center>
      </div>
      <div class="card-body">
  
  
        <table class="table table-bordered table-striped">
          <thead style="background-color: lightblue">
            <tr>
              <th scope="col">Minggu</th>
              <th scope="col">Total Uang Masuk</th>
              <th scope="col">Total Uang Keluar</th>
              <th scope="col">Saldo</th>
             
            </tr>
          </thead>
          <tbody>
              @php
                  $masukok = 0;
              @endphp
                  @foreach ($masuk as $item)
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '1')
                        @php
                          $masukok += $item->uangmasuk;
                        @endphp
                          
                      @else
  
                      @endif
                  @endforeach
  
              @php
                  $keluarok = 0;
              @endphp
                  @foreach ($keluar as $item)
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '1')
                        @php
                          $keluarok += $item->uangkeluar;
                        @endphp
                          
                      @else
                      @endif
  
                  @endforeach
                      @php
                          $totalok = $masukok-$keluarok;
                      @endphp
                      <tr>
                        <td>1</td>
                        <td><p> @currency($masukok)</p></td>
                        <td><p> @currency($keluarok)</p></td>
                        <td><p> @currency($totalok)</p></td>
                      </tr>
  
                  {{-- minggu kedua --}}
                      
              @php
                  $masukok2 = 0;
              @endphp
                  @foreach ($masuk as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '2')
                        @php
                          $masukok2 += $item->uangmasuk;
                        @endphp
                          
                      @else
                      @endif
                  @endforeach
  
                      @php
                      $keluarok2 = 0;
                      @endphp
                      @foreach ($keluar as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '2')
                        @php
                          $keluarok2 += $item->uangkeluar;
                        @endphp
                          
                      @else
                      @endif
                      @endforeach
                      @php
                          $totalok2 = $masukok2-$keluarok2;
                      @endphp
                      <tr>
                        <td>2</td>
                        <td><p> @currency($masukok2)</p></td>
                        <td><p> @currency($keluarok2)</p></td>
                        <td><p> @currency($totalok2)</p></td>
                      </tr>
  
  
              {{-- minggu ke tiga --}}
              @php
                  $masukok3 = 0;
              @endphp
                  @foreach ($masuk as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '3')
                        @php
                          $masukok3 += $item->uangmasuk;
                        @endphp
                          
                      @else
                      @endif
                  @endforeach
  
                      @php
                      $keluarok3 = 0;
                      @endphp
                      @foreach ($keluar as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '3')
                        @php
                          $keluarok3 += $item->uangkeluar;
                        @endphp
                          
                      @else
                      @endif
                      @endforeach
                      @php
                          $totalok3 = $masukok3-$keluarok3;
                      @endphp
                      <tr>
                        <td>3</td>
                        <td><p> @currency($masukok3)</p></td>
                        <td><p> @currency($keluarok3)</p></td>
                        <td><p> @currency($totalok3)</p></td>
                      </tr>
  
               {{-- minggu keempat --}}
              @php
                  $masukok4 = 0;
              @endphp
                  @foreach ($masuk as $item)
   
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '4')
                      @php
                      $masukok4 += $item->uangmasuk;
                      @endphp
                   
                      @else
                      @endif
                  @endforeach
  
                      @php
                      $keluarok4 = 0;
                      @endphp
                      @foreach ($keluar as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '4')
                      @php
                          $keluarok4 += $item->uangkeluar;
                      @endphp
                          
                      @else
                      @endif
                      @endforeach
                      @php
                          $totalok4 = $masukok4-$keluarok4;
                      @endphp
                      <tr>
                      <td>4</td>
                      <td><p> @currency($masukok4)</p></td>
                      <td><p> @currency($keluarok4)</p></td>
                      <td><p> @currency($totalok4)</p></td>
                      </tr>
      
             
           
          </tbody>
        </table>
      
      </div>
    </div>
    
    
    @else
        
    @endif
   
</div>
@endsection