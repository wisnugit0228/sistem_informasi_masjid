@extends('layouts.main')

@section('content')
@if ($detail->nama === null)
    <div class="container">
      <div class="card">
        <div class="card-body">
          <center>
            <h2>Silahkan Lengkapi Profile</h2>
            <a href="/profile/{{ $detail->id }}/edit" class="btn btn-warning">Edit</a>
          </center>
        </div>
      </div>
    </div>
@else
<section class="vh-50" style="background-color: #f4f5f7;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col col-lg-6 mb-4 mb-lg-0">
        <div class="card mb-3" style="border-radius: .5rem;">
          <div class="row g-0">
            <div class="col-md-4 gradient-custom text-center text-white"
              style="border-top-left-radius: .5rem; border-bottom-left-radius: .5rem;">
              <img class="rounded-circle mt-5" src="/fotoprofile/{{ $detail->foto }}"
                alt="Avatar" class="img-fluid my-5" style="width: 80px;" />
                
              <h5>{{ $detail->nama }}</h5>
              <p>{{ $detail->role }}</p>
              <a href="/profile/{{ $detail->id }}/edit" class="btn btn-warning">Edit</a>

            </div>
            <div class="col-md-8">
              <div class="card-body p-4">
                <h6>Information</h6>
                <hr class="mt-0 mb-4">
                <div class=" pt-1">
                  <div class="col-8 mb-3">
                    <h6>Tempat, Tanggal Lahir</h6>
                    <p class="text-muted">{{ $detail->tempat }}, {{ $detail->tanggal }}</p>
                  </div>
                  <div class="col-6 mb-3">
                    <h6>Jenis Kelamin</h6>
                    <p class="text-muted">{{ $detail->gender }}</p>
                  </div>
                  <div class="col-6 mb-3">
                      <h6>Alamat</h6>
                      <p class="text-muted">{{ $detail->alamat }}</p>
                    </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    
@endif

@endsection