@extends('layouts.main')

@section('content')

<center>
    <h3>Data Admin</h3>
</center>

<div class="container">
   
    <table class="table table-bordered">
        
          <tr>
            <th scope="col">Nama</th>
            <td>{{ $saran->nama }}</td>
          </tr>
          <tr>
              <th>Email</th>
              <td>{{ $saran->email }}</td>
          </tr>
          <tr>
              <th>Konten</th>
              <td>{{ $saran->konten }}</td>
          </tr>
      
      </table>
</div>

@endsection