<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masuk;
use App\Models\Keluar;
use App\Models\Masjid;
// use App\Http\Controllers\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CetakController extends Controller
{
    public function cetakmasuk()
    {
        $idmasjid = Auth::user()->profile->masjid->id;
        $masuk = Masuk::get()->where('masjid_id', $idmasjid);

        return view('cetak.saldomasuk',['masuk'=>$masuk]);
    }

    public function carimasuk(Request $request)
    {
        $idmasjid = Auth::user()->profile->masjid->id;
        $cari = $request->cari;
 
		$masuk = DB::table('masuk')
        ->where('masjid_id', $idmasjid)
		->where('bulan','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('cetak.cetaksaldomasuk',['masuk' => $masuk]);
    }

    public function cetakkeluar()
    {
        $idmasjid = Auth::user()->profile->masjid->id;
        $keluar = Keluar::get()->where('masjid_id', $idmasjid);

        return view('cetak.saldokeluar',['keluar'=>$keluar]);
    }

    public function carikeluar(Request $request)
    {
        $idmasjid = Auth::user()->profile->masjid->id;
        $cari = $request->cari;
 
		$keluar = DB::table('keluar')
        ->where('masjid_id', $idmasjid)
		->where('bulan','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('cetak.cetaksaldokeluar',['keluar' => $keluar]);
    }

    public function caridatamasuk(Request $request)
    {
        $idmasjid = Auth::user()->profile->masjid->id;
        $cari = $request->cari;
 
		$masuk = DB::table('masuk')
        ->where('masjid_id', $idmasjid)
		->where('bulan','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('cetak.saldomasuk',['masuk' => $masuk]);
    }

    public function caridatakeluar(Request $request)
    {
        $idmasjid = Auth::user()->profile->masjid->id;
        $cari = $request->cari;
 
		$keluar = DB::table('keluar')
        ->where('masjid_id', $idmasjid)
		->where('bulan','like',"%".$cari."%")
		->paginate();
 
    		// mengirim data pegawai ke view index
		return view('cetak.saldokeluar',['keluar' => $keluar]);
    }
}
