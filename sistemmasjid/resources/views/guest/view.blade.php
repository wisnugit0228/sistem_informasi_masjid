@extends('layouts.main2')

@section('nav')
<ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
    <li class="nav-item"><a class="nav-link" href="#masjid">Masjid</a></li>
    <li class="nav-item"><a class="nav-link" href="#tentang">Tentang</a></li>
    <li class="nav-item"><a class="nav-link" href="#about">Kontak</a></li>
</ul>
@endsection

@section('content')

<div class="container">
    <div>
        <hr>
        <center>
            <h3 id="masjid">Daftar Masjid Desa Tebat Agung</h3>

        </center>
            
        <hr>

        <div class="row">
            @foreach ($masjid as $item)
            @if ($item->nama === 'Super Admin')

            <div class="col-lg-4" style="display: none">
                <div class="card">
                   <a href="/masjid/{{ $item->id }}">
                       <img src="/fotomasjid/{{ $item->foto }}" class="card-img-top" alt="...">
                        <div class="card-body" style="background-color: #ececec">
                        <p class="card-text" style="color: black; text-decoration:none;font-family: Arial, Helvetica, sans-serif">{{ $item->nama }}</p>
                    </a> 
                        </div>
                </div>

            </div>
                
            @else

            <div class="col-lg-4">
                <div class="card">
                   <a href="/masjid/{{ $item->id }}">
                       <img src="/fotomasjid/{{ $item->foto }}" class="card-img-top" alt="...">
                        <div class="card-body" style="background-color: #ececec">
                        <p class="card-text" style="color: black; text-decoration:none;font-family: Arial, Helvetica, sans-serif">{{ $item->nama }}</p>
                    </a> 
                        </div>
                </div>

            </div>
                
            @endif
              
           
                
            @endforeach
            
        </div>
    </div>


    <div class="mt-5">
        <hr>
        <center>
            <h3 id="tentang">Tentang</h3>
        </center>
        <hr>

        <div class="row">
            <div class="col-lg-8">
                <p style="font-family: Arial, Helvetica, sans-serif">
                    Masjid (serapan dari Arab: مَسْجِد, translit. masjid, diucapkan [mǝsdʒid]; secara harfiah "tempat sujud"), 
                    merupakan tempat salat bagi umat Islam.Masjid biasanya tertutup bangunan, tetapi bisa menjadi tempat 
                    salat (sujud) dilakukan, termasuk halaman luar. <br>

                    Awalnya masjid adalah tempat salat sederhana bagi umat Islam, dan mungkin merupakan ruang terbuka daripada bangunan.Pada 
                    tahap pertama arsitektur Islam, 650-750, masjid terdiri dari ruang terbuka dan tertutup yang dikelilingi oleh dinding, 
                    seringkali dengan menara tempat azan dikeluarkan. Bangunan masjid biasanya berisi mihrab dipasang di dinding yang menunjukkan
                    arah Kiblat ke Makkah, dan fasilitas wudu. Mimbar, tempat di mana khutbah salat Jumat disampaikan, dulunya adalah
                    karakteristik masjid pusat kota, tetapi sejak itu menjadi umum di masjid-masjid kecil.Masjid biasanya memiliki ruang
                    terpisah untuk pria dan wanita. Pola dasar organisasi ini mengambil bentuk yang berbeda tergantung pada wilayah, periode,
                        dan mazhab. <br>

                </p>
            </div>
            <div class="col-lg-4">

                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <img src="/images/masjidbagus1.jpg" class="d-block w-100" alt="...">
                      </div>
                      <div class="carousel-item">
                        <img src="/images/masjidbagus2.jpg" class="d-block w-100" alt="...">
                      </div>
                      <div class="carousel-item">
                        <img src="/images/masjidbagus3.jpg" class="d-block w-100" alt="...">
                      </div>
                    </div>
                   <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </button>
                  </div>

            </div>

            <div class="col-lg-12">
                <p style="font-family: Arial, Helvetica, sans-serif">
                    Masjid umumnya berfungsi sebagai lokasi untuk salat, buka puasa Ramadan, salat Jenazah, pelaksanaan pernikahan dan bisnis,
                    pengumpulan dan distribusi sedekah, serta tempat penampungan tunawisma. Secara historis, masjid telah berfungsi 
                    sebagai pusat komunitas, pengadilan, dan sekolah agama. Di zaman modern, mereka juga mempertahankan perannya sebagai 
                    tempat pengajaran dan debat agama. Kepentingan khusus diberikan kepada Masjidilharam (pusat haji), Masjid Nabawi di Madinah 
                    (tempat pemakaman Muhammad) dan Masjidilaqsa di Yerusalem (diyakini sebagai tempat kenaikan Muhammad ke surga). <br>

                    Dengan penyebaran Islam, masjid berlipat ganda di seluruh dunia Islam. Terkadang gereja dan kuil diubah menjadi masjid,
                    yang memengaruhi gaya arsitektur Islam. Sementara sebagian besar masjid pra-modern didanai oleh sumbangan amal, 
                    peningkatan peraturan pemerintah tentang masjid besar telah diimbangi dengan munculnya masjid yang didanai swasta, banyak 
                    di antaranya berfungsi sebagai basis untuk berbagai organisasi revivalis Islam dan aktivitas sosial. Masjid telah 
                    memainkan sejumlah peran politik. Tingkat kehadiran masjid sangat bervariasi tergantung pada wilayah.
                </p>
            </div>
        </div>
    </div>






    <div class="mt-5">
        <hr>
        <center>
            <h3 id="kontak">Kontak</h3>
        </center>
        <hr>

        <div>
            <form action="/saran" method="POST" enctype="multipart/form-data">
                @csrf
            
             
            
                  <div class="form-group col-xl-6 col-lg-8">
                      <label for="nama">nama</label>
                      <input type="text" class="form-control" name="nama" id="nama"  placeholder="Masukan nama">
                  </div>
                  @error('nama')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror


                  <div class="form-group col-xl-6 col-lg-8">
                    <label for="email">email</label>
                    <input type="email" class="form-control" name="email" id="email"  placeholder="Masukan email">
                </div>
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
        
        
                  
                  
                  <div class="form-group col-xl-6 col-lg-8">
                      <label for="konten">Konten</label>
                      <textarea class="form-control" id="konten" name="konten" rows="3" placeholder="masukan saran"></textarea>
                      
                  </div>
                  @error('konten')
                  <div class="alert alert-danger">{{ $message }}</div>
              
                  @enderror
        
        
            <div class="container">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
                  
                  
                  
                  
            </form>
        </div>
        
        
    </div>



</div>
    
@endsection