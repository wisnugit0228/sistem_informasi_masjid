<?php

namespace App\Http\Controllers;

use App\Models\Keluar;
use App\Models\Masjid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SaldoKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Keluar::where('masjid_id', $idmasjid)->first();
        $keluar = Keluar::get()->where('masjid_id', $idmasjid);
        

        return view('keluar.view', ['keluar'=>$keluar], ['mypost'=>$mypost]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id;
    
        return view ('keluar.tambah', ['masjid'=>$masjid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'uangkeluar' => 'required',
            'keterangan'=> 'required',
            'tgl'=> 'required',
            'minggu'=> 'required',
            'bulan'=> 'required',
            'masjid_id'=> 'required',
            
        ]);



        $keuangan = new Keluar;
        $keuangan->uangkeluar = $request->uangkeluar;
        $keuangan->keterangan = $request->keterangan;
        $keuangan->tgl = $request->tgl;
        $keuangan->minggu = $request->minggu;
        $keuangan->bulan = $request->bulan;
        $keuangan->masjid_id = $request->masjid_id;

        $keuangan->save();

        return redirect('/cetaksaldokeluar');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Keluar  $keluar
     * @return \Illuminate\Http\Response
     */
    public function show(Keluar $keluar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Keluar  $keluar
     * @return \Illuminate\Http\Response
     */
    public function edit(Keluar $keluar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Keluar  $keluar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keluar $keluar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Keluar  $keluar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keluar $keluar)
    {
        //
    }
}
