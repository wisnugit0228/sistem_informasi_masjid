@extends('layouts.main')

@section('content')
<form action="/profile/{{ $detail->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')

    <div class="">
        <img src="/fotoprofile/{{ $detail->foto }}" class="img-thumbnail" alt="...">
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="nama" class="form-label">Foto Profile</label>
        
        <input type="file" class="form-control" id="foto" name="foto" value="{{ $detail->foto }}">
    </div>
    <div class="form-group col-xl-6 col-lg-8">
      <label for="nama" class="form-label">Nama</label>
      @if ($detail->nama === null)
        <input type="text" class="form-control" id="nama" name="nama"  value=" " placeholder="masukan nama" >  
      @else
        <input type="text" class="form-control" id="nama" name="nama"  value="{{ $detail->nama }}" placeholder="masukan nama" >
      @endif
      
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="tempat" class="form-label">Tempat Lahir</label>
        @if ($detail->tempat === null)
            <input type="text" class="form-control" id="tempat" name="tempat" value=" " placeholder="masukan tempat" >  
            
        @else
            <input type="text" class="form-control" id="tempat" name="tempat" value="{{ $detail->tempat }}" placeholder="masukan tempat" >
        @endif
        
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="tanggal" class="form-label">Tanggal Lahir</label>
        @if ($detail->tanggal === null)
            <input type="date" class="form-control" id="tanggal" name="tanggal" value=" "> 
        @else
            <input type="date" class="form-control" id="tanggal" name="tanggal" value="{{ $detail->tanggal }}">
        @endif
        
    </div>


    <div class="form-group col-xl-6 col-lg-8">
        <label for="gender" class="form-label">Jenis Kelamin</label>
        <select name="gender" id="gender" class="form-control" >
            @if ($detail->gender)
                    <option value="">--pilih jenis kelamin--</option>
                    <option value="pria">Pria</option>
                    <option value="wanita">Wanita</option>
                @if ($detail->gender === $detail->gender)
                    <option value="{{ $detail->gender }}" selected>{{ $detail->gender }}</option>
                @else
                    <option value="pria">Pria</option>
                    <option value="wanita">Wanita</option>
                @endif
            @else
                <option value="Pilih jenis kelamin" selected>-</option>
                <option value="pria">Pria</option>
                <option value="wanita">Wanita</option>
            @endif
            
            
        </select>
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="nohp" class="form-label">Telepon</label>
        @if ($detail->hohp === null)
            <input type="number" class="form-control" id="nohp" name="nohp" value="{{ $detail->nohp }}">  
            
        @else
            <input type="number" class="form-control" id="nohp" name="nohp" value="{{ $detail->nohp }}">
        @endif
        
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="alamat" class="form-label">Alamat</label>
        @if ($detail->alamat === null)
        <textarea name="alamat" id="alamat" class="form-control" placeholder="masukan alamat"> </textarea>

        @else
        <textarea name="alamat" id="alamat" class="form-control" placeholder="masukan alamat">{{ $detail->alamat }}</textarea>
        @endif
        
    </div>

    

    <div class="form-group col-xl-6 col-lg-8">
        
        
        <input type="text" class="form-control" id="role" name="role" value="Admin" style="display: none">
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        
        
        <input type="text" class="form-control" id="role" name="user_id" value="{{ $detail->user->id }}" style="display: none">
    </div>

    

   
    
    <button type="submit" class="btn btn-primary form-group">Submit</button>
  </form>
@endsection

