@extends('layouts.main3')


@section('nav')
<div class="container">
    {{-- <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="..." /></a> --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ms-1"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item"><a class="nav-link" href="/masjid/{{ $idmasjid->masjid_id }}">Beranda</a></li>
           
        </ul>
    </div>
</div>
@endsection


@section('content')
    
    <hr>
    <center>
        <h3>Jadwal Sholat Masjid</h3>
        
    </center>
    <hr>

    <div class="card">
     
      <div class="card-body">
  
  
        <table class="table table-bordered table-striped">
          <thead style="background-color: lightblue">
            <tr>
              <th scope="col">Tanggal</th>
              <th scope="col">Imam</th>
              <th scope="col">Khotib</th>
              <th scope="col">Muadzin</th>
             
            </tr>
          </thead>
          <tbody>
              @foreach ($jadwal as $item)
              @if ($item->bulan === $mytime->format('Y-m'))
      
              <tr>
              
                <td>Jumat, {{ $item->tgl }}</td>
                <td>{{ $item->imam }}</td>
                <td>{{ $item->khotib }}</td>
                <td>{{ $item->muadzin }}</td>
             
              </tr>
                  
              @else
      
              @endif
             
                  
             
      
              @endforeach
           
          </tbody>
        </table>
      
      </div>
    </div>
    
    

@endsection









