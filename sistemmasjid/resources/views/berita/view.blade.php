@extends('layouts.main')

@section('content')

<div class="container">
    @if ($mypost=$mypost)
    <a href="/berita/create" class="btn btn-info">berita</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Judul</th>
            <th scope="col">Konten</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @php
                $no = 1;
            @endphp
            @foreach ($berita as $item)

            <tr>
                <th scope="row">@php
                   echo $no; 
                @endphp</th>
                <td>{{ $item->judul }}</td>
                <td>{{ $item->konten }}</td>
                <td>
                    <form action="/berita/{{ $item->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="berita/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a> 
                        <button type="submit" class="btn btn-danger btn-sm" id="delete" >hapus</button>
                        <a href="/berita/{{ $item->id }}" ><button type="button" class="btn btn-primary btn-sm">detail</button></a> 
                      </form>
                </td>
              </tr>
                @php
                    $no++
                @endphp
            @endforeach
         
        </tbody>
      </table>
    @else
        <a href="/berita/create" class="btn btn-info">Tambah Berita</a>
    @endif
   
</div>
    
@endsection

