@extends('layouts.main')

@section('content')
<div class="container">
    @if ($mypost=$mypost)
    @auth
    <a href="/saldomasuk/create" class="btn btn-info">Tambah Data</a>

    <a href="/cetaksaldomasuk" class="btn btn-warning">Cetak</a>
    
    @endauth
    
    <br><br>

    {{-- Bulan Oktober --}}
    <h3 class="container mt-3">Laporan Uang Masuk Tahun 2023</h3>
    <table class="table table-bordered table-striped">
        <thead style="background-color: lightskyblue">
          <tr>
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($masuk as $item)
          <tr>
            <td>{{ $item->tgl }}</td>
            <td>@currency($item->uangmasuk)</td>
            <td>{{ $item->keterangan }}</td>
          </tr>
          @empty
              
          @endforelse
         
         
        </tbody>
      </table>
      



     
    @else
    @auth
    <a href="/saldomasuk/create" class="btn btn-info">Tambah Data</a>
    @endauth
        
    @endif
   
</div>
@endsection