<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluar', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('uangkeluar');
            $table->string('keterangan');
            $table->string('tgl');
            $table->string('minggu');
            $table->string('bulan');
            $table->unsignedBigInteger('masjid_id');
            $table->foreign('masjid_id')->references('id')->on('masjid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keluar');
    }
}
