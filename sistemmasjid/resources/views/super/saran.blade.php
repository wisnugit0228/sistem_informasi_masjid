@extends('layouts.main')

@section('content')

<center>
    <h3>Data Admin</h3>
</center>

<div class="container">
    <a href="/register" class="btn btn-primary">Tambah Admin</a>
    <table class="table table-bordered table-striped">
        <thead style="background-color: lightskyblue">
          <tr>
            <th scope="col">Nama</th>
            <th scope="col">Email</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($data as $item)
          <tr>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->email }}</td>
           <td ><a href="/saran/{{ $item->id }}">Lihat</a></td>
          </tr>
         
         
          @empty
              
          @endforelse
         
         
        </tbody>
      </table>
</div>

@endsection