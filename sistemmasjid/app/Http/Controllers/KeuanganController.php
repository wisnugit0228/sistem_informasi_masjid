<?php

namespace App\Http\Controllers;

use App\Models\Keluar;
use App\Models\Masuk;
use App\Models\Masjid;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

class KeuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        $mypost = Masuk::where('masjid_id', $idmasjid)->first();
        $mypostt = Keluar::where('masjid_id', $idmasjid)->first();

        $masuk = Masuk::get()->where('masjid_id', $idmasjid);
        $keluar = Keluar::get()->where('masjid_id', $idmasjid);
        $mytime = Carbon::now();
       
        // $keuangan = Keuangan::get()->where('masjid_id', $idmasjid);
        

        return view('keuangan.view', [
            'mypostt'=>$mypostt,
            'mypost'=>$mypost,
            'masuk'=>$masuk,
            'keluar'=>$keluar,
            'mytime'=>$mytime
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $masjid = Auth::user()->profile->masjid->id;
        // $maks = Keuangan::get()->where('masjid_id', $masjid)->last();
        
        // return view ('keuangan.tambah', ['masjid'=>$masjid], ['maks'=>$maks]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'tgl' => 'required',
        //     'masuk'=> 'required',
        //     'keluar'=> 'required',
        //     'saldo'=> 'required',
        //     'masjid_id'=> 'required',
        //     'rincian'=> 'required',
        //     'bulan'=> 'required'
            
        // ]);



        // $keuangan = new Keuangan;
        // $keuangan->tgl = $request->tgl;
        // $keuangan->masuk = $request->masuk;
        // $keuangan->keluar = $request->keluar;
        // $keuangan->saldo = $request->saldo;
        // $keuangan->rincian = $request->rincian;
        // $keuangan->masjid_id = $request->masjid_id;
        // $keuangan->bulan = $request->bulan;

        // $keuangan->save();

        // return redirect('keuangan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Keuangan  $keuangan
     * @return \Illuminate\Http\Response
     */
    public function show(Keuangan $keuangan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Keuangan  $keuangan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $masjid = Auth::user()->profile->masjid->id;
        // $maks = Keuangan::get()->where('masjid_id', $masjid)->last();
        // $keuangan = Keuangan::find($id);
        // $masjid = Masjid::get();
        // return view ('keuangan.edit', ['keuangan'=>$keuangan], ['maks'=>$maks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Keuangan  $keuangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keuangan $keuangan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Keuangan  $keuangan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keuangan $keuangan)
    {
        //
    }
}
