<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class TestController extends Controller
{
    public function tampil()
    {
        $detail = Profile::get();
        return view('guest.tes', ['detail'=>$detail]);
    }
}
