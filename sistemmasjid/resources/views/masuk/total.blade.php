@extends('layouts.main')

@section('content')
<div class="container">
    @if ($mypost=$mypost)
    <a href="/totalsaldomasuk/create" class="btn btn-info">Tambah Data</a>
    <h3 class="container mt-3">Laporan Bulan Oktober</h3>
    <table class="table">
        <thead>
          <tr>
            
            <th scope="col">Minggu Ke</th>
            <th scope="col">Total Uang Masuk</th>
          </tr>
        </thead>
        <tbody>
            
            @php
                $total = 0;
            @endphp

            @foreach ($masuk as $item)
                
           
            @php
                $total += $item->uangmasuk;
            @endphp
            @endforeach
            <tr>
                <td>1</td>
                <td><p>@currency($total)</p></td>
            </tr>

            
           
            

            {{-- <tr>
                <td>{{ 1 }}</td>
               
            
                <td>
                @foreach ($masuk as $item)
                 {{ ($item->uangmasuk) }}
                 @endforeach
                </td>
            
            
            </tr> --}}
            

           
               
            
         
        </tbody>
      </table>



      {{-- <h3 class="container mt-3">Laporan Bulan November</h3>
    <table class="table">
        <thead>
          <tr>
            
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Uang Keluar</th>
            <th scope="col">Saldo</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
           
            @foreach ($keuangan as $item)

            @if ($item->bulan === '2023-11')
            <tr>
               
              <td>Jumat, {{ $item->tgl }}</td>
              <td>{{ $item->masuk }}</td>
              <td>{{ $item->keluar }}</td>
              <td>{{ $item->saldo }}</td>
              <td>
                      <a href="keuangan/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a>   
                    
              </td>
            </tr>
                
            @else
                
            @endif

            
               
            @endforeach
         
        </tbody>
      </table>

      <h3 class="container mt-3">Laporan Bulan Desember</h3>
    <table class="table">
        <thead>
          <tr>
            
            <th scope="col">Tanggal</th>
            <th scope="col">Uang Masuk</th>
            <th scope="col">Uang Keluar</th>
            <th scope="col">Saldo</th>
            <th scope="col">Rincian</th>
          </tr>
        </thead>
        <tbody>
           
            @foreach ($keuangan as $item)

            @if ($item->bulan === '2023-12')
            <tr>
               
              <td>Jumat, {{ $item->tgl }}</td>
              <td>{{ $item->masuk }}</td>
              <td>{{ $item->keluar }}</td>
              <td>{{ $item->saldo }}</td>
              <td>
                      <a href="keuangan/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a>   
                    
              </td>
            </tr>
                
            @else
                
            @endif

          
               
            @endforeach
         
        </tbody>
      </table> --}}
    @else
        <a href="/totalsaldomasuk/create" class="btn btn-info">Tambah Data</a>
    @endif
   
</div>
@endsection