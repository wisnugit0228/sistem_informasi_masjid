<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('profile.view', ['detail'=>$detail]);
        // $detail = Profile::get();
        // return view('profile.view', ['detail'=>$detail]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $iduser = Auth::id();
        $detail = Profile::where('user_id', $iduser)->first();
        return view('profile.edit', ['detail'=>$detail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpg,png,jpeg',
            'nama' => 'required',
            'tempat'=> 'required',
            'tanggal' => 'required',
            'gender' => 'required',
            'nohp' => 'required',
            'alamat' => 'required',
            'role' => 'required'
            
        ]);

        $post = Profile::find($id);

        if ($request->has('foto')) {
            $patch = 'images/';
            File::delete($patch.$post->foto);

            $fileName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('fotoprofile'), $fileName);

            $post->foto = $fileName;


            $post->save();

        }

        $post->nama = $request['nama'];
        $post->tempat = $request['tempat'];
        
        $post->tanggal = $request['tanggal'];
        $post->gender = $request['gender'];
        $post->nohp = $request['nohp'];
        $post->alamat = $request['alamat'];
        $post->role = $request['role'];

        $post->save();

        return redirect('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
