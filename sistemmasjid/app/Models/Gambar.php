<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    use HasFactory;
    protected $table = 'gambar';
    protected $fillable = ['foto','kategori', 'masjid_id'];

    public function masjid(){
        return $this->belongsTo(Masjid::class, 'masjid_id');
    }
}
