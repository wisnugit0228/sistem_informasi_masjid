@extends('layouts.main3')


@section('nav')
<div class="container">
    {{-- <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="..." /></a> --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ms-1"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item"><a class="nav-link" href="#tentang">Tentang</a></li>
            <li class="nav-item"><a class="nav-link" href="#berita">Berita</a></li>
            <li class="nav-item"><a class="nav-link" href="#galeri">Galeri</a></li>
            <li class="nav-item"><a class="nav-link" href="/daftarpetugas/{{ $masjid->id }}/daftar">Anggota</a></li>
            <li class="nav-item"><a class="nav-link" href="/jadwaljumat/{{ $masjid->id }}/jadwal">Petugas Sholat Jumat</a></li>
            <li class="nav-item"><a class="nav-link" href="/laporankeuangan/{{ $masjid->id }}/daftar">Laporan Keuangan</a></li>
        </ul>
    </div>
</div>
@endsection


@section('content')
    
    <div class="container-fluid col-lg-12">
        <div class="card bg-success col-lg-12">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6 mr-5">
                  <b class="">Jadwal Sholat</b>
                  <h6><b>Di Kecamatan Rambang Niru, {{ $mytime->format('D M Y') }}</b></h6>
    
                </div>
               
               <div class="kotak mr-2">
                <center>
                  <b style="color: black">Subuh</b>
                  <p style="color: black">{{ $sholat->subuh }}</p>
                </center>
               </div>
    
               <div class="kotak mr-2">
                <center>
                  <b style="color: black">Dzuhur</b>
                  <p style="color: black">{{ $sholat->dzuhur }}</p>
                </center>
               </div>
    
    
               <div class="kotak mr-2">
                <center>
                  <b style="color: black">Ashar</b>
                  <p style="color: black">{{ $sholat->ashar }}</p>
                </center>
               </div>
    
    
               <div class="kotak mr-2">
                <center>
                  <b style="color: black">Magrib</b>
                  <p style="color: black">{{ $sholat->magrib }}</p>
                </center>
               </div>
    
    
               <div class="kotak mr-2">
                <center>
                  <b style="color: black">Isya</b>
                  <p style="color: black">{{ $sholat->isya }}</p>
                </center>
               </div>
                
              </div>
            </div>
          </div>
        
          <div>

            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="/fotomasjid/{{ $masjid->foto }}" class="d-block w-100" alt="...">
                  </div>
                  @foreach ($foto as $item)
                  <div class="carousel-item">
                    <img src="/galeri/{{ $item->foto }}" class="d-block w-100" alt="...">
                  </div>
                      
                  @endforeach
                  
                  
                </div>
               <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </button>
              </div>
              <br><br>
              <hr>
              <center>
                  <h3 id="tentang">{{ $masjid->nama }}</h3>
              </center>
              
              <hr>

              <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <p><b>Tentang Masjid:</b></p>
                            <p>{{ $masjid->tentang }}</p>

                            <p><b>Lokasi:</b></p>
                            @if ($masjid->nama === 'Masjid Jamik Darussalam')
                            <iframe class="col-lg-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.560252941193!2d104.06112071055118!3d-3.4565068965034214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e3a3add184d4669%3A0x6555d4aa3f180682!2sMasjid%20Jami&#39;k%20Darussalam!5e0!3m2!1sid!2sid!4v1701828169392!5m2!1sid!2sid" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            @else
                              @if ($masjid->nama === 'Masjid Alhidayah')
                                <iframe class="col-lg-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7040.631266604251!2d104.02521200412635!3d-3.45212199628264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e3a3bd99be2acfd%3A0x559958fc59471691!2sMasjid%20Al-Hidayah!5e0!3m2!1sid!2sid!4v1701767440311!5m2!1sid!2sid" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                              @else
                                  
                              @endif
                                @if ($masjid->nama === 'Masjid Darul Hikmah')
                                <iframe class="col-lg-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.475159715115!2d104.07454741055122!3d-3.4767159964830654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e3a254a7dc26989%3A0xda8c29bfe9b88b20!2sMasjid%20Darul%20Hikmah!5e0!3m2!1sid!2sid!4v1701828443749!5m2!1sid!2sid"  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                                @else
                                    
                                @endif
                            
                                
                            @endif
                            
                            
                        </div>
                      </div>
                    
                    
                   

                </div>
                <div class="col-lg-8">
                   
                    <img src="/fotomasjid/{{ $masjid->foto }}" class="img-thumbnail col-lg-12" alt="...">
                </div>
              </div>

              <br><br>
              <hr>
              <center>
                  <h3 id="berita">Berita Seputar {{ $masjid->nama }}</h3>
              </center>
              <hr>

              <div>
                
                       
                        <div class="row">
                          @foreach ($berita->take(4) as $item)
                            <div class="col-lg-3">
                                <div class="card">
                                    <div style="max-height: 150px; overflow:hidden;">
                                      <img src="/fotoberita/{{ $item->foto }}"  class="card-img-top" alt="...">
                                    </div>
                                    
                                    <div class="card-body">
                                      <h5 class="card-title"><b>{{ $item->judul }}</b></h5>
                                      <p class="foto card-text"><span>...</span></p>
                                      <a href="/berita/{{ $item->id }}" class="btn btn-success">Lihat</a>
                                    </div>
                                  </div>

                            </div>
                          
            
                          @endforeach
            
                        </div>
                       
            
                    
                
                  <center>
                    <a href="/fullberita/{{ $masjid->id }}/daftar" class="btn btn-outline-success mt-2">Selengkapnya></a>
                   
                  </center>
              </div>

              <br><br>
              <hr>
              <center>
                  <h3 id="galeri">Galeri {{ $masjid->nama }}</h3>
              </center>
              <hr>

              <div>
                
                        <div class="row">
                         
                          @foreach ($galeri->take(4) as $item)

                          <div class="col-lg-3">
                            <div class="card">
                                <div style="max-height: 120px; overflow:hidden;">
                                  <img src="/galeri/{{ $item->foto }}" class="card-img-top" alt="...">
                                </div>
                              </div>

                          </div>
                        
                           
                         
                          
            
                          @endforeach
            
                         
            
                        </div>
                      
                  <center>
                    <a href="/fullgaleri/{{ $masjid->id }}/daftar" class="btn btn-outline-success mt-2">Selengkapnya></a>
                 
                  </center>
              </div>
    </div>

@endsection