@extends('layouts.main3')


@section('nav')
<div class="container">
    {{-- <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="..." /></a> --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ms-1"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item"><a class="nav-link" href="/masjid/{{ $berita->masjid_id }}">Beranda</a></li>
          
        </ul>
    </div>
</div>
@endsection


@section('content')
    
    <hr>
    <center>
        <h6>{{ $berita->judul }}</h6>
        
    </center>
    <hr>

    <div class="row">
        <div class="col-lg-4">
            <div class="col-lg-12">
                <img src="/fotoberita/{{ $berita->foto }}" class="img-thumbnail" alt="...">
    
            </div>

        </div>

        <div class="col-lg-8">
            <div class="col-lg-9">
                
                <p> {{ $berita->konten }} </p><br>
            </div>

        </div>
        
        
        
    </div>
    

@endsection


