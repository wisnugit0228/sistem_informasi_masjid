@extends('layouts.main')


@section('content')
    <form action="/sholat" method="POST" enctype="multipart/form-data">
        @csrf
    
       
    
          <div class="form-group col-xl-6 col-lg-8">
              <label for="subuh">Subuh</label>
              <input type="time" class="form-control" name="subuh" id="subuh"  placeholder="Masukan subuh">
          </div>
          @error('subuh')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          <div class="form-group col-xl-6 col-lg-8">
            <label for="dzuhur">Dzuhur</label>
            <input type="time" class="form-control" name="dzuhur" id="dzuhur"  placeholder="Masukan dzuhur">
        </div>
        @error('dzuhur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group col-xl-6 col-lg-8">
            <label for="ashar">Ashar</label>
            <input type="time" class="form-control" name="ashar" id="ashar"  placeholder="Masukan ashar">
        </div>
        @error('ashar')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group col-xl-6 col-lg-8">
            <label for="magrib">Magrib</label>
            <input type="time" class="form-control" name="magrib" id="magrib"  placeholder="Masukan magrib">
        </div>
        @error('magrib')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group col-xl-6 col-lg-8">
            <label for="isya">Isya</label>
            <input type="time" class="form-control" name="isya" id="isya"  placeholder="Masukan isya">
        </div>
        @error('isya')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror


          
          
          

        <input type="text" name="masjid_id" value="{{ $masjid }}" style="display: none">
    
          
          <button type="submit" class="btn btn-primary">Submit</button>
          
          
    </form>
@endsection