<?php

namespace App\Http\Controllers;

use App\Models\Masuk;
use App\Models\Masjid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SaldomasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Masuk::where('masjid_id', $idmasjid)->first();
        $masuk = Masuk::get()->where('masjid_id', $idmasjid);
        

        return view('masuk.view', ['masuk'=>$masuk], ['mypost'=>$mypost]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id;
    
        return view ('masuk.tambah', ['masjid'=>$masjid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'uangmasuk' => 'required',
            'keterangan'=> 'required',
            'tgl'=> 'required',
            'minggu'=> 'required',
            'bulan'=> 'required',
            'masjid_id'=> 'required',
            
        ]);



        $keuangan = new Masuk;
        $keuangan->uangmasuk = $request->uangmasuk;
        $keuangan->keterangan = $request->keterangan;
        $keuangan->tgl = $request->tgl;
        $keuangan->minggu = $request->minggu;
        $keuangan->bulan = $request->bulan;
        $keuangan->masjid_id = $request->masjid_id;

        $keuangan->save();

        return redirect('/cetaksaldomasuk');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Masuk  $masuk
     * @return \Illuminate\Http\Response
     */
    public function show(Masuk $masuk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Masuk  $masuk
     * @return \Illuminate\Http\Response
     */
    public function edit(Masuk $masuk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Masuk  $masuk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Masuk $masuk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Masuk  $masuk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Masuk $masuk)
    {
        //
    }
}
