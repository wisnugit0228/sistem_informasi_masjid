@extends('layouts.main')

@section('content')
    <div class="container">
        @if ($mypost=$mypost)
        <a href="/anggota/create" class="btn btn-info">Tambah Anggota</a>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Status</th>
                <th scope="col">Alamat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($anggota as $item)

                <tr>
                    <th scope="row">@php
                       echo $no; 
                    @endphp</th>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->status }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>
                        <form action="/anggota/{{ $item->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="anggota/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a> 
                            <button type="submit" class="btn btn-danger btn-sm" id="delete" >hapus</button>
                            <a href="/anggota/{{ $item->id }}" ><button type="button" class="btn btn-primary btn-sm">detail</button></a> 
                          </form>
                    </td>
                  </tr>
                    @php
                        $no++
                    @endphp
                @endforeach
             
            </tbody>
          </table>
        @else
            <a href="/anggota/create" class="btn btn-info">Tambah Anggota</a>
        @endif
       
    </div>
@endsection