<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalsholatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwalsholat', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('subuh');
            $table->string('dzuhur');
            $table->string('ashar');
            $table->string('magrib');
            $table->string('isya');
            $table->unsignedBigInteger('masjid_id');
            $table->foreign('masjid_id')->references('id')->on('masjid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwalsholat');
    }
}
