<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('tgl');
            $table->string('imam');
            $table->string('khotib');
            $table->string('muadzin');
            $table->string('bulan');
            $table->unsignedBigInteger('masjid_id');
            $table->foreign('masjid_id')->references('id')->on('masjid');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal');
    }
}
