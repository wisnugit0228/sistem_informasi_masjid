<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Masjid</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset ('/template/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset ('/template/dist/css/adminlte.min.css') }}">

  <link rel="stylesheet" href="{{ asset ('/css/style.css') }}">

  <link rel="stylesheet" href="{{ asset ('/css/style1.css') }}">

  
 
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  @include('partials.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary bg-success elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="/images/icon.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Masjid Tebat Agung</span>
    </a>
  
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
     
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/images/admin.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Super Admin</a>
        </div>
      </div>
    
     
  
      <!-- SidebarSearch Form -->
  
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
  
          
  
          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                  <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                </svg>
              </i>
              <p>
                User Admin
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                  <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                </svg>
              </i>
              <p>
                Masukan
              </p>
            </a>
          </li>
  
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"></a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
         
          <div>
            <table class="table table-bordered table-striped">
                <thead style="background-color: lightblue">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Konten</th>
                   
                   
                  </tr>
                </thead>
                <tbody>
                    
            
                    <tr>     
                        <td>1</td>
                        <td>masjidjamik</td>
                        <td>masjidjamik@gmail.com</td>
                        <td>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vel alias error ipsa cupiditate nulla aliquid dolorum, magnam maiores expedita culpa nostrum, saepe accusamus iste reiciendis soluta architecto! Tempore quasi autem velit nesciunt quidem provident facere ducimus quam eveniet possimus explicabo consequatur magnam commodi ullam incidunt sed, deleniti eligendi, voluptatem minima maiores. Quae vero recusandae error quidem ab labore quaerat accusantium. Provident impedit reprehenderit nihil obcaecati consectetur amet doloremque illum sapiente quia assumenda quasi tempore, error culpa magnam fugiat quisquam? Earum fuga culpa quod itaque dolorum optio? Saepe, unde labore nobis esse voluptatum molestiae cum corporis sed quia hic incidunt perspiciatis?</td>
                     
                     
                      </tr>
                      <tr>     
                        <td>1</td>
                        <td>masjidjamik</td>
                        <td>masjidjamik@gmail.com</td>
                        <td>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vel alias error ipsa cupiditate nulla aliquid dolorum, magnam maiores expedita culpa nostrum, saepe accusamus iste reiciendis soluta architecto! Tempore quasi autem velit nesciunt quidem provident facere ducimus quam eveniet possimus explicabo consequatur magnam commodi ullam incidunt sed, deleniti eligendi, voluptatem minima maiores. Quae vero recusandae error quidem ab labore quaerat accusantium. Provident impedit reprehenderit nihil obcaecati consectetur amet doloremque illum sapiente quia assumenda quasi tempore, error culpa magnam fugiat quisquam? Earum fuga culpa quod itaque dolorum optio? Saepe, unde labore nobis esse voluptatum molestiae cum corporis sed quia hic incidunt perspiciatis?</td>
                     
                     
                      </tr><tr>     
                        <td>1</td>
                        <td>masjidjamik</td>
                        <td>masjidjamik@gmail.com</td>
                        <td>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vel alias error ipsa cupiditate nulla aliquid dolorum, magnam maiores expedita culpa nostrum, saepe accusamus iste reiciendis soluta architecto! Tempore quasi autem velit nesciunt quidem provident facere ducimus quam eveniet possimus explicabo consequatur magnam commodi ullam incidunt sed, deleniti eligendi, voluptatem minima maiores. Quae vero recusandae error quidem ab labore quaerat accusantium. Provident impedit reprehenderit nihil obcaecati consectetur amet doloremque illum sapiente quia assumenda quasi tempore, error culpa magnam fugiat quisquam? Earum fuga culpa quod itaque dolorum optio? Saepe, unde labore nobis esse voluptatum molestiae cum corporis sed quia hic incidunt perspiciatis?</td>
                     
                     
                      </tr>
                        
                   
                   
                        
                   
            
                 
                </tbody>
              </table>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
         
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      {{-- <b>Version</b> 3.2.0 --}}
    </div>
    <strong>Copyright &copy; 2023 Masjid Desa Tebat Agung.</strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset ('/template/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset ('/template/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{asset ('/template/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset ('/template/dist/js/demo.js') }}"></script>
{{-- drop down --}}
<script src="{{ asset ('/js/dropdown.js') }}"></script>
{{-- Hitung Otomastis --}}
<script src="{{ asset ('/js/hitung.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>
</html>
