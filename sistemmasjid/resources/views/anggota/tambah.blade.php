@extends('layouts.main')


@section('content')
    <form action="/anggota" method="POST" enctype="multipart/form-data">
        @csrf
    
        <div class="form-group col-xl-6 col-lg-8">
            <label for="foto" class="form-label">Foto</label>
            <input type="file" class="form-control" id="foto" name="foto"  >
        </div>
    
          <div class="form-group col-xl-6 col-lg-8">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" name="nama" id="nama"  placeholder="Masukan Nama">
          </div>
          @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror


          
          
          <div class="form-group col-xl-6 col-lg-8">
              <label for="alamat">Alamat</label>
              <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>
              
          </div>
          @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
      
          @enderror

          <div class="form-group col-xl-6 col-lg-8">
            <label for="status" class="form-label">Status Anggota</label>
            <select name="status" id="status" class="form-control" >
                        <option value="">--pilih Status Anggota--</option>
                        <option value="Imam">Imam</option>
                        <option value="Khotib">Khotib</option>
                        <option value="Muadzin">Muadzin</option>
                        <option value="Pengurus">Pengurus</option>
            </select>
        </div>

         

        <input type="text" name="masjid_id" value="{{ $masjid }}" style="display: none">
    
          
          <button type="submit" class="btn btn-primary">Submit</button>
          
          
    </form>
@endsection