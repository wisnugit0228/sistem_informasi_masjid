@extends('layouts.main')

@section('content')

<center>
    <h3>Data Admin</h3>
</center>

<div class="container">
    <a href="/register" class="btn btn-primary">Tambah Admin</a>
    <table class="table table-bordered table-striped">
        <thead style="background-color: lightskyblue">
          <tr>
            <th scope="col">User Name</th>
            <th scope="col">Email</th>
            <th scope="col">Admin Masjid</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($data as $item)
          <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->email }}</td>
           <td>{{ $item->profile->masjid->nama }}</td>
           <td style="color: green">Aktif</td>
          </tr>
         
         
          @empty
              
          @endforelse
         
         
        </tbody>
      </table>
</div>

@endsection