<?php

namespace App\Http\Controllers;

use App\Models\Gambar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class GambarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Gambar::where('masjid_id', $idmasjid)->first();
        $foto = Gambar::get()->where('masjid_id', $idmasjid)->where('kategori', 'gambar masjid');
        $galeri = Gambar::get()->where('masjid_id', $idmasjid)->where('kategori', 'galeri');
        
        return view('gambar.view', ['foto'=>$foto,'mypost'=>$mypost,'galeri'=>$galeri]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id;
    
        return view ('gambar.tambah', ['masjid'=>$masjid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg',
            'kategori' => 'required',
            'masjid_id'=> 'required'
            
        ]);


        $fileName = time().'.'.$request->foto->extension();
        $request->foto->move(public_path('galeri'), $fileName);

        $masjid = new Gambar;
        $masjid->foto = $fileName;
        
        $masjid->kategori = $request->kategori;
        $masjid->masjid_id = $request->masjid_id;

        $masjid->save();

        return redirect('gambar');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function show(Gambar $gambar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function edit(Gambar $gambar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gambar $gambar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gambar  $gambar
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Gambar::find($id);
        $patch = 'galeri/';
        File::delete($patch.$berita->foto);

        $berita->delete();

        return redirect('gambar');
    }
}
