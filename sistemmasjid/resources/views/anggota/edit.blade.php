@extends('layouts.main')

@section('content')
<form action="/anggota/{{ $anggota->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')

    <div class="col-4">
        <h3>Foto Anggota</h3>
        <img src="/fotoanggota/{{ $anggota->foto }}" class="img-thumbnail" alt="...">
    </div>


    <div class="form-group col-xl-6 col-lg-8">
        <label for="foto" class="form-label">Foto</label>
        <input type="file" class="form-control" id="foto" name="foto"  >
    </div>

      <div class="form-group col-xl-6 col-lg-8">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" name="nama" id="nama"  value="{{ $anggota->nama }}">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      
      
      <div class="form-group col-xl-6 col-lg-8">
          <label for="alamat">Alamat</label>
          <textarea class="form-control" id="alamat" name="alamat" rows="3">{{ $anggota->alamat }}</textarea>
          
      </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
  
      @enderror

      <div class="form-group col-xl-6 col-lg-8">
        <label for="status" class="form-label">Status Anggota</label>
        <select name="status" id="status" class="form-control" >
            @if ($anggota->status === $anggota->status)
                <option value="{{ $anggota->status }}" selected>{{ $anggota->status }}</option>
                    <option value="Imam">Imam</option>
                    <option value="Khotib">Khotib</option>
                    <option value="Muadzin">Muadzin</option>
                    <option value="Pengurus">Pengurus</option>
                
            @else

            
                
            @endif
                    
        </select>
    </div>

     

    <input type="text" name="masjid_id" value="{{ $anggota->masjid_id }}">

      
      <button type="submit" class="btn btn-primary">Submit</button>

   
  </form>
@endsection