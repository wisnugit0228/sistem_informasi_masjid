<?php

namespace App\Http\Controllers;

use App\Models\Masjid;
use App\Models\Anggota;
use App\Models\Jadwal;
use App\Models\Keuangan;
use App\Models\Masuk;
use App\Models\Keluar;
use App\Models\Berita;
use App\Models\Gambar;
use App\Models\Sholat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use File;


class MasjidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $iduser = Auth::id();
        // $detail = Profile::where('user_id', $iduser)->first();
        // return view('profile.view', ['detail'=>$detail]);

        $idprofile =  Auth::user()->profile->id;
        // $category = Profile::get();
        // $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Masjid::where('profile_id', $idprofile)->first();
        $masjid = Masjid::get()->where('profile_id', $idprofile)->first();
        $foto = Gambar::get()->where('masjid_id', $idprofile)->where('kategori', 'gambar masjid');
        $mypost1 = Sholat::where('masjid_id', $idprofile)->first();
        $sholat = Sholat::get()->where('masjid_id', $idprofile)->first();
        $mytime = Carbon::now();
      
        

        return view('masjid.view', [
                                    'masjid'=>$masjid, 
                                    'mypost'=>$mypost, 
                                    'foto'=>$foto, 
                                    'mypost1'=>$mypost1, 
                                    'sholat'=>$sholat,
                                    'mytime'=>$mytime
    ]);
        // $masjid = Masjid::get();
        // return view('masjid.view', ['masjid'=>$masjid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = Auth::user()->profile->id; 
        return view ('masjid.tambah', ['profile'=>$profile]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg',
            'nama' => 'required',
            'alamat'=> 'required',
            'link'=> 'required',
            'tentang'=> 'required',
            'profile_id'=> 'required'
            
        ]);


        $fileName = time().'.'.$request->foto->extension();
        $request->foto->move(public_path('fotomasjid'), $fileName);

        $masjid = new Masjid;
        $masjid->foto = $fileName;
        $masjid->nama = $request->nama;
        $masjid->alamat = $request->alamat;
        $masjid->link = $request->link;
        $masjid->tentang = $request->tentang;
        $masjid->profile_id = $request->profile_id;

        $masjid->save();

        return redirect('masjid');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Masjid  $masjid
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $maxid = DB::table('jadwal')->max('id');
        // $maksid = Jadwal::get('tgl')->max('id');

        $keuangan = Keuangan::get()->where('masjid_id', $id);
        $jadwal = Jadwal::get()->where('masjid_id', $id);
        $anggota = Anggota::get()->where('masjid_id', $id);
        $masuk = Masuk::get()->where('masjid_id', $id);
        $keluar = Keluar::get()->where('masjid_id', $id);
        $masjid = Masjid::get()->where('id', $id)->first();
        $berita = Berita::get()->where('masjid_id', $id);
        $mytime = Carbon::now();
        $last = Masuk::get()->where('masjid_id', $id)->last();
        $foto = Gambar::get()->where('masjid_id', $id)->where('kategori', 'gambar masjid');
        $galeri = Gambar::get()->where('masjid_id', $id)->where('kategori', 'galeri');
        $hitungfoto = Gambar::get()->where('masjid_id', $id)->where('kategori', 'gambar masjid')->count();
        $hitunggaleri = 0;
        $sholat = Sholat::get()->where('masjid_id', $id)->first();
 
        return view('guest.detail', 
        ['masjid'=>$masjid,
        'anggota'=>$anggota,
        'jadwal'=>$jadwal,
        'keuangan'=>$keuangan,
        'masuk'=>$masuk,
        'keluar'=>$keluar,
        'berita'=>$berita,
        'mytime'=>$mytime,
        'last'=>$last,
        'foto'=>$foto,
        'galeri'=>$galeri,
        'hitungfoto'=>$hitungfoto,
        'hitunggaleri'=>$hitunggaleri,
        'sholat'=>$sholat
  
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Masjid  $masjid
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        return view('masjid.ubah', ['masjid'=>$masjid]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Masjid  $masjid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'foto' => 'image|mimes:jpg,png,jpeg',
            'alamat' => 'required',
            'link' => 'required',
            'tentang'=> 'required',
            'profile_id'=> 'required'
        ]);

        $masjid = Masjid::find($id);
        if ($request->has('foto')) {
            $patch = 'fotomasjid/';
            File::delete($patch.$masjid->foto);

            $fileName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('fotomasjid'), $fileName);

            $masjid->foto = $fileName;


            $masjid->save();

        }

        $masjid->nama = $request['nama'];
        $masjid->alamat = $request['alamat'];
        $masjid->link = $request['link'];
        $masjid->tentang = $request['tentang'];
        $masjid->profile_id = $request['profile_id'];
        $masjid->save();

        return redirect('masjid');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Masjid  $masjid
     * @return \Illuminate\Http\Response
     */
    public function destroy(Masjid $masjid)
    {
        //
    }
}
