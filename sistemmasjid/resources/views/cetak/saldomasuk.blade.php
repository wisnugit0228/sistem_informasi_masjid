@extends('layouts.main')

@section('content')
    <div class="container">
        <a href="/saldomasuk/create" class="btn btn-info">Tambah Data</a>
        <br><br>
        <div class="row">
            <div class="col-lg-6">
                <form action="/saldomasuk/cari" method="GET">
                    <div class="row">
                        <select name="cari" id="" class="form-control col-lg-4">
                            <option value="">---</option>
                            <option value="2023-12">Desember</option>
                            <option value="2023-11">November</option>
                            <option value="2023-10">Oktober</option>
                            <option value="2023-09" disabled>September</option>
                            <option value="2023-08" disabled>Agustus</option>
                            <option value="2023-07" disabled>Juli</option>
                            <option value="2023-06" disabled>Juni</option>
                            <option value="2023-05" disabled>Mei</option>
                            <option value="2023-04" disabled>April</option>
                            <option value="2023-03" disabled>Maret</option>
                            <option value="2023-02" disabled>Februari</option>
                            <option value="2023-01" disabled>Januari</option>
                        </select>
                        <input type="submit" value="CARI" class="btn btn-warning col-lg-2">
                    </div>
                </form>

            </div>

            <div class="col-lg-6">
                <form action="/cetaksaldomasuk/cari" method="GET">
                    <div class="row">
                        <select name="cari" id="" class="form-control col-lg-4">
                            <option value="">---</option>
                            <option value="2023-12">Desember</option>
                            <option value="2023-11">November</option>
                            <option value="2023-10">Oktober</option>
                            <option value="2023-09" disabled>September</option>
                            <option value="2023-08" disabled>Agustus</option>
                            <option value="2023-07" disabled>Juli</option>
                            <option value="2023-06" disabled>Juni</option>
                            <option value="2023-05" disabled>Mei</option>
                            <option value="2023-04" disabled>April</option>
                            <option value="2023-03" disabled>Maret</option>
                            <option value="2023-02" disabled>Februari</option>
                            <option value="2023-01" disabled>Januari</option>
                        </select>
                        <input type="submit" value="CETAK" class="btn btn-warning col-lg-2">
                    </div>
                </form>

            </div>
        </div>
        
        <br>
        
        <table class="table table-bordered table-striped">
            <thead style="background-color: lightskyblue">
              <tr>
                <th scope="col" style="text-align: center">Tanggal</th>
                <th scope="col" style="text-align: center">Uang Masuk</th>
                <th scope="col" style="text-align: center">Rincian</th>
              </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                @endphp
              @forelse ($masuk as $item)
                @php
                    $total += $item->uangmasuk;
                @endphp
              <tr>
                <td>{{ $item->tgl }}</td>
                <td>@currency($item->uangmasuk)</td>
                <td>{{ $item->keterangan }}</td>
              </tr>
              @empty
                  
              @endforelse
              <td colspan="3" style="text-align: center"><b>Total Uang Masuk : @currency($total)</b></td>
             
             
            </tbody>
          </table>
    </div>
@endsection