@extends('layouts.main')


@section('content')
    <form action="/masjid/{{ $masjid->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="container">
            <img src="/fotomasjid/{{ $masjid->foto }}" class="img-thumbnail" alt="..." width="500">
        </div>
    
        <div class="form-group col-xl-6 col-lg-8">
            <label for="foto" class="form-label">Foto</label>
            <input type="file" class="form-control" id="foto" name="foto"  >
        </div>
    
          <div class="form-group col-xl-6 col-lg-8">
              <label for="nama">Nama Masjid</label>
              <input type="text" class="form-control" name="nama" id="nama"  placeholder="Masukan Nama" value="{{ $masjid->nama }}">
          </div>
          @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror


          
          
          <div class="form-group col-xl-6 col-lg-8">
              <label for="alamat">Alamat</label>
              <textarea class="form-control" id="alamat" name="alamat" rows="3">{{ $masjid->alamat }}</textarea>
              
          </div>
          @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
      
          @enderror

          <div class="form-group col-xl-6 col-lg-8">
            <label for="link">Link</label>
            <textarea class="form-control" id="link" name="link" rows="3">{{ $masjid->link }}</textarea>
            
        </div>
        @error('link')
        <div class="alert alert-danger">{{ $message }}</div>
    
        @enderror

          <div class="form-group col-xl-6 col-lg-8">
            <label for="tentang">Tentang</label>
            <textarea class="form-control" id="tentang" name="tentang" rows="3">{{ $masjid->tentang }}</textarea>
            
        </div>
        @error('tentang')
        <div class="alert alert-danger">{{ $message }}</div>
    
        @enderror

        <input type="text" name="profile_id" value="{{ $masjid->profile_id }}" style="display: none">
    
          
          <button type="submit" class="btn btn-primary">Submit</button>
          
          
    </form>
@endsection