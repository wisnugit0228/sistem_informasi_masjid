<?php

namespace App\Http\Controllers;

use App\Models\Totalmasuk;
use App\Models\Masuk;
use App\Models\Masjid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TotalmasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Masuk::where('masjid_id', $idmasjid)->first();
        $masuk = Masuk::get()->where('masjid_id', $idmasjid);
        $total = Totalmasuk::get()->where('masjid_id', $idmasjid);
        

        return view('masuk.total', ['total'=>$total, 'mypost'=>$mypost, 'masuk'=>$masuk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Totalmasuk  $totalmasuk
     * @return \Illuminate\Http\Response
     */
    public function show(Totalmasuk $totalmasuk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Totalmasuk  $totalmasuk
     * @return \Illuminate\Http\Response
     */
    public function edit(Totalmasuk $totalmasuk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Totalmasuk  $totalmasuk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Totalmasuk $totalmasuk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Totalmasuk  $totalmasuk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Totalmasuk $totalmasuk)
    {
        //
    }
}
