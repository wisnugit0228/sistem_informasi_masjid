@extends('layouts.main')


@section('content')
    <form action="/gambar" method="POST" enctype="multipart/form-data">
        @csrf
    
        <div class="form-group col-xl-6 col-lg-8">
            <label for="foto" class="form-label">Foto Galeri</label>
            <input type="file" class="form-control" id="foto" name="foto"  >
        </div>
    
          <div class="form-group col-xl-6 col-lg-8">
              <label for="kategori">Kategori</label>
              <select name="kategori" id="kategori" class="form-control">
                  <option value="gambar masjid">Gambar Masjid</option>
                  <option value="galeri">Galeri</option>
              </select>
          </div>
          @error('kategori')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        

        <input type="text" name="masjid_id" value="{{ $masjid }}" style="display: none">
    
          
          <button type="submit" class="btn btn-primary">Submit</button>
          
          
    </form>
@endsection