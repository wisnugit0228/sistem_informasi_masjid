<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Totalmasuk extends Model
{
    use HasFactory;
    protected $table = 'saldomasuk';

    protected $fillable = ['saldo', 'tgl', 'minggu', 'bulan', 'masjid_id'];
}
