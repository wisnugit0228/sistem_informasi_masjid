<?php

namespace App\Http\Controllers;

use App\Models\Anggota;
use App\Models\Masjid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Anggota::where('masjid_id', $idmasjid)->first();
        $anggota = Anggota::get()->where('masjid_id', $idmasjid);
        

        return view('anggota.view', ['anggota'=>$anggota], ['mypost'=>$mypost]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id; 
        return view ('anggota.tambah', ['masjid'=>$masjid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'foto' => 'required|image|mimes:jpg,png,jpeg',
            'nama' => 'required',
            'alamat'=> 'required',
            'status'=> 'required',
            'masjid_id'=> 'required'
            
        ]);


        $fileName = time().'.'.$request->foto->extension();
        $request->foto->move(public_path('fotoanggota'), $fileName);

        $masjid = new Anggota;
        $masjid->foto = $fileName;
        $masjid->nama = $request->nama;
        $masjid->alamat = $request->alamat;
        $masjid->status = $request->status;
        $masjid->masjid_id = $request->masjid_id;

        $masjid->save();

        return redirect('anggota');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggota = Anggota::get()->where('id', $id)->first();

        return view('anggota.detail', ['anggota'=>$anggota]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota = Anggota::find($id);
        $masjid = Masjid::get();
        return view ('anggota.edit', ['anggota'=>$anggota], ['masjid'=>$masjid]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'foto' => 'image|mimes:jpg,png,jpeg',
            'nama' => 'required',
            'alamat'=> 'required',
            'status' => 'required',
            'masjid_id' => 'required'
            
        ]);

        $post = Anggota::find($id);

        if ($request->has('foto')) {
            $patch = 'images/';
            File::delete($patch.$post->foto);

            $fileName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('fotoanggota'), $fileName);

            $post->foto = $fileName;


            $post->save();

        }

        $post->nama = $request['nama'];
        $post->alamat = $request['alamat'];
        
        $post->status = $request['status'];
        $post->masjid_id = $request['masjid_id'];

        $post->save();

        return redirect('anggota');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Anggota  $anggota
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggota = Anggota::find($id);
        $patch = 'images_content/';
        File::delete($patch.$anggota->thumbnail);

        $anggota->delete();

        return redirect('anggota');
    }
}
