<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masjid;
use App\Models\Jadwal;
use App\Models\Masuk;
use App\Models\Keluar;
use App\Models\Anggota;
use App\Models\Gambar;
use App\Models\Berita;
use Carbon\Carbon;

class GuestController extends Controller
{
    public function index()
    {
        // $jadwal = Jadwal::get()->where()
        $masjid = Masjid::get();
        return view('guest.view', ['masjid'=>$masjid]);
    }

    public function lihat($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();

        return view('guest.detail', ['masjid'=>$masjid]);
    }

    public function rinciansaldo($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $masuk = Masuk::get()->where('masjid_id', $id);
        return view('guest.rincian', ['masuk'=>$masuk]);
    }

    public function rinciansaldokeluar($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $keluar = Keluar::get()->where('masjid_id', $id);
        return view('guest.rinciankeluar', ['keluar'=>$keluar]);
    }

    public function jadwal($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $jadwal = Jadwal::get()->where('masjid_id', $id);
        $idmasjid = Jadwal::get()->where('masjid_id', $id)->first();
        $mytime = Carbon::now();
        return view('guest.jadwal', ['jadwal'=>$jadwal, 'mytime'=>$mytime, 'idmasjid'=>$idmasjid]);
    }

    public function petugas($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $petugas = Anggota::get()->where('masjid_id', $id);
        $idmasjid = Anggota::get()->where('masjid_id', $id)->first();
        $mytime = Carbon::now();
        return view('guest.petugas', ['petugas'=>$petugas, 'mytime'=>$mytime, 'idmasjid'=>$idmasjid]);
    }

    public function laporan($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $masuk = Masuk::get()->where('masjid_id', $id);
        $keluar = Keluar::get()->where('masjid_id', $id);
        $idmasjid = Keluar::get()->where('masjid_id', $id)->first();
        $mytime = Carbon::now();
        return view('guest.laporan', ['masuk'=>$masuk, 'keluar'=>$keluar, 'mytime'=>$mytime, 'idmasjid'=>$idmasjid]);
    }

    public function galerifull($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $galeri = Gambar::get()->where('masjid_id', $id)->where('kategori', 'galeri');
        $idmasjid = Gambar::get()->where('masjid_id', $id)->where('kategori', 'galeri')->first();
        return view('guest.galerifull', ['galeri'=>$galeri, 'idmasjid'=>$idmasjid]);
    }

    public function beritafull($id)
    {
        $masjid = Masjid::get()->where('id', $id)->first();
        $berita = Berita::get()->where('masjid_id', $id);
        $idmasjid = Berita::get()->where('masjid_id', $id)->first();
        return view('guest.beritafull', ['berita'=>$berita, 'idmasjid'=>$idmasjid]);
    }
}
