<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sholat extends Model
{
    use HasFactory;
    protected $table = 'jadwalsholat';
    protected $fillable = ['subuh','dzuhur', 'ashar', 'magrib', 'isya', 'masjid_id'];

    public function masjid(){
        return $this->belongsTo(Masjid::class, 'masjid_id');
    }
}
