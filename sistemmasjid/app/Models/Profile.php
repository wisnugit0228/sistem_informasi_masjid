<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Users;

class Profile extends Model
{
    use HasFactory;
    protected $table = 'profile';

    protected $fillable = ['nama','foto', 'tempat', 'tanggal', 'gender', 'nohp', 'alamat', 'role', 'user_id'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function masjid(){
        return $this->hasOne(Masjid::class, 'profile_id');

    }

    

}
