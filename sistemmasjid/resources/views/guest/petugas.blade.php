@extends('layouts.main3')


@section('nav')
<div class="container">
    {{-- <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="..." /></a> --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ms-1"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item"><a class="nav-link" href="/masjid/{{ $idmasjid->masjid_id }}">Beranda</a></li>
           
        </ul>
    </div>
</div>
@endsection


@section('content')
    
    <hr>
    <center>
        <h3>Daftar Anggota Masjid</h3>
        
    </center>
    <hr>
<div class="card">


    <div class="card-body">


      <table class="table table-bordered table-striped">
        <thead style="background-color: lightblue">
          <tr>
            <th scope="col">Foto</th>
            <th scope="col">Nama</th>
            <th scope="col">Sebagai</th>
            <th scope="col">Alamat</th>
           
          </tr>
        </thead>
        <tbody>
            @foreach ($petugas as $item)
    
            <tr>     
                <td><img src="/fotoanggota/{{ $item->foto }}" alt="" width="50"></td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->status }}</td>
                <td>{{ $item->alamat }}</td>
             
              </tr>
                
           
           
                
           
    
            @endforeach
         
        </tbody>
      </table>
    
    </div>

  </div>
    

@endsection

