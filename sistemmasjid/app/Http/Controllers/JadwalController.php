<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use App\Models\Masjid;
use App\Models\Anggota;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idmasjid =  Auth::user()->profile->masjid->id;
        
        $mypost = Jadwal::where('masjid_id', $idmasjid)->first();
        $jadwal = Jadwal::get()->where('masjid_id', $idmasjid);
        $mytime = Carbon::now();
        

        return view('jadwal.view', ['jadwal'=>$jadwal, 'mypost'=>$mypost, 'mytime'=>$mytime]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masjid = Auth::user()->profile->masjid->id;

        $kh = Anggota::get()->where('status', 'Khotib');
        $i = Anggota::get()->where('status', 'Imam');
        $m = Anggota::get()->where('status', 'Muadzin');
        $mytime = Carbon::now();
        // $masjid = Auth::user()->profile->masjid->id;
        // $k = Anggota::get('status')->where('status', 'Khotib');
        // // $imam = Anggota::get()->where('status', 'Imam');
        // // // $khotib = DB::table('anggota')->where('status', 'imam');
        
        // // $muadzin = Anggota::where('status', 'Muadzin'); 
        // return view ('jadwal.tambah', ['masjid'=>$masjid], ['khotib'=>$k]);
        return view ('jadwal.tambah', 
        [
            'kh'=>$kh, 
            'i'=>$i, 
            'm'=>$m, 
            'masjid'=>$masjid,
            'mytime'=>$mytime
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl' => 'required',
            'imam'=> 'required',
            'khotib'=> 'required',
            'muadzin'=> 'required',
            'bulan'=> 'required',
            'masjid_id'=> 'required',
            
        ]);


        $jadwal = new Jadwal;
        $jadwal->tgl = $request->tgl;
        $jadwal->imam = $request->imam;
        $jadwal->khotib = $request->khotib;
        $jadwal->muadzin = $request->muadzin;
        $jadwal->bulan = $request->bulan;
        $jadwal->masjid_id = $request->masjid_id;

        $jadwal->save();

        return redirect('jadwal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jadwal = Jadwal::find($id);
        $kh = Anggota::get()->where('status', 'Khotib');
        $i = Anggota::get()->where('status', 'Imam');
        $m = Anggota::get()->where('status', 'Muadzin');

        return view ('jadwal.edit', 
        [
            'kh'=>$kh, 
            'i'=>$i, 
            'm'=>$m, 
            'jadwal'=>$jadwal
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tgl' => 'required',
            'imam' => 'required',
            'khotib'=> 'required',
            'muadzin' => 'required',
            'masjid_id' => 'required'
            
        ]);

        $post = Jadwal::find($id);

        $post->tgl = $request['tgl'];
        $post->imam = $request['imam'];
        $post->khotib = $request['khotib'];
        $post->muadzin = $request['muadzin'];
        $post->masjid_id = $request['masjid_id'];

        $post->save();

        return redirect('jadwal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Jadwal::find($id);
        
        

        $berita->delete();

        return redirect('jadwal');
    }
}
