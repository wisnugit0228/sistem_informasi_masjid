@extends('layouts.main')

@section('content')
<form action="/jadwal/{{ $jadwal->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')


      <div class="form-group col-xl-6 col-lg-8">
          <label for="tgl">Tanggal</label>
          <input type="text" class="form-control" name="tgl" id="tgl"  value="{{ $jadwal->tgl }}">
      </div>
      @error('tgl')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      


      <div class="form-group col-xl-6 col-lg-8">
        <label for="imam" class="form-label">Imam</label>
        <select name="imam" id="imam" class="form-control" >
            @if ($jadwal->imam === $jadwal->imam)
                <option value="{{ $jadwal->imam }}" selected>{{ $jadwal->imam }}</option>
                @foreach ($i as $imam)

                <option value="{{ $imam->nama }}">{{ $imam->nama }}</option>
                    
                @endforeach
                
            @else
            
                
            @endif
                    
        </select>
    </div>

    <div class="form-group col-xl-6 col-lg-8">
        <label for="khotib" class="form-label">Khotib</label>
        <select name="khotib" id="khotib" class="form-control" >
            @if ($jadwal->khotib === $jadwal->khotib)
                <option value="{{ $jadwal->khotib }}" selected>{{ $jadwal->khotib }}</option>
                @foreach ($kh as $khotib)

                <option value="{{ $khotib->nama }}">{{ $khotib->nama }}</option>
                    
                @endforeach
                
            @else
            
                
            @endif
                    
        </select>
    </div>


    <div class="form-group col-xl-6 col-lg-8">
        <label for="muadzin" class="form-label">Muadzin</label>
        <select name="muadzin" id="muadzin" class="form-control" >
            @if ($jadwal->muadzin === $jadwal->muadzin)
                <option value="{{ $jadwal->muadzin }}" selected>{{ $jadwal->muadzin }}</option>
                @foreach ($m as $muadzin)

                <option value="{{ $muadzin->nama }}">{{ $muadzin->nama }}</option>
                    
                @endforeach
                
            @else
            
                
            @endif
                    
        </select>
    </div>

     

    <input type="text" name="masjid_id" value="{{ $jadwal->masjid_id }}">

      
      <button type="submit" class="btn btn-primary">Submit</button>

   
  </form>
@endsection