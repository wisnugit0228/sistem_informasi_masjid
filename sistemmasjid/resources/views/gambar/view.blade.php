@extends('layouts.main')

@section('content')
    <div class="container">
        @if ($mypost=$mypost)
        <h3>Foto Masjid</h3>
        <a href="/gambar/create" class="btn btn-info">Tambah Foto</a>
        <br>
        <table class="table table-bordered">
            <thead style="background-color: lightblue">
              <tr>
                
                <th scope="col">Foto</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($foto as $item)

                <tr>
                    <td>
                        
                            <img src="/galeri/{{ $item->foto }}" width="200" alt="...">
                    </td>
                   
                    <td>
                        <form action="/gambar/{{ $item->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            {{-- <a href="anggota/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a>  --}}
                            <button type="submit" class="btn btn-danger btn-sm" id="delete" >hapus</button>
                            {{-- <a href="/gambar/{{ $item->id }}" ><button type="button" class="btn btn-primary btn-sm">detail</button></a>  --}}
                          </form>
                    </td>
                  </tr>
                    @php
                        $no++
                    @endphp
                @endforeach
             
            </tbody>
          </table>
        @else
            <a href="/gambar/create" class="btn btn-info">Tambah Foto</a>
        @endif


        <h3>Foto Galeri</h3>
        <a href="/gambar/create" class="btn btn-info">Tambah Foto</a>
        <br>

        <table class="table table-bordered">
          <thead style="background-color: lightblue">
            <tr>
              
              <th scope="col">Foto</th>
              <th scope="col">Aksi</th>
            </tr>
          </thead>
          <tbody>
              @php
                  $no = 1;
              @endphp
              @foreach ($galeri as $item)

              <tr>
                  <td>
                      
                          <img src="/galeri/{{ $item->foto }}" width="200" alt="...">
                  </td>
                 
                  <td>
                      <form action="/gambar/{{ $item->id }}" method="POST">
                          @csrf
                          @method('DELETE')
                          {{-- <a href="anggota/{{ $item->id }}/edit" ><button type="button" class="btn btn-warning btn-sm">edit</button></a>  --}}
                          <button type="submit" class="btn btn-danger btn-sm" id="delete" >hapus</button>
                          {{-- <a href="/gambar/{{ $item->id }}" ><button type="button" class="btn btn-primary btn-sm">detail</button></a>  --}}
                        </form>
                  </td>
                </tr>
                  @php
                      $no++
                  @endphp
              @endforeach
           
          </tbody>
        </table>
       
    </div>
@endsection