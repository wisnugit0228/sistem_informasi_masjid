@extends('layouts.main3')


@section('nav')
<div class="container">
    {{-- <a class="navbar-brand" href="#page-top"><img src="assets/img/navbar-logo.svg" alt="..." /></a> --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars ms-1"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
            <li class="nav-item"><a class="nav-link" href="/masjid/{{ $idmasjid->masjid_id }}">Beranda</a></li>
         
        </ul>
    </div>
</div>
@endsection


@section('content')
    
    <hr>
    <center>
        <h3>Laporan Keuangann Bulan Ini</h3>
        
    </center>
    <hr>

    <div class="card">
      
      <div class="card-body">
  
  
        <table class="table table-bordered table-striped">
          <thead style="background-color: lightblue">
            <tr>
              <th scope="col">Minggu</th>
              <th scope="col">Total Uang Masuk</th>
              <th scope="col">Total Uang Keluar</th>
              <th scope="col">Saldo</th>
             
            </tr>
          </thead>
          <tbody>
              @php
                  $masukok = 0;
              @endphp
                  @foreach ($masuk as $item)
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '1')
                        @php
                          $masukok += $item->uangmasuk;
                        @endphp
                          
                      @else
  
                      @endif
                  @endforeach
  
              @php
                  $keluarok = 0;
              @endphp
                  @foreach ($keluar as $item)
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '1')
                        @php
                          $keluarok += $item->uangkeluar;
                        @endphp
                          
                      @else
                      @endif
  
                  @endforeach
                      @php
                          $totalok = $masukok-$keluarok;
                      @endphp
                      <tr>
                        <td>1</td>
                        <td><p> @currency($masukok)</p></td>
                        <td><p> @currency($keluarok)</p></td>
                        <td><p> @currency($totalok)</p></td>
                      </tr>
  
                  {{-- minggu kedua --}}
                      
              @php
                  $masukok2 = 0;
              @endphp
                  @foreach ($masuk as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '2')
                        @php
                          $masukok2 += $item->uangmasuk;
                        @endphp
                          
                      @else
                      @endif
                  @endforeach
  
                      @php
                      $keluarok2 = 0;
                      @endphp
                      @foreach ($keluar as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '2')
                        @php
                          $keluarok2 += $item->uangkeluar;
                        @endphp
                          
                      @else
                      @endif
                      @endforeach
                      @php
                          $totalok2 = $masukok2-$keluarok2;
                      @endphp
                      <tr>
                        <td>2</td>
                        <td><p> @currency($masukok2)</p></td>
                        <td><p> @currency($keluarok2)</p></td>
                        <td><p> @currency($totalok2)</p></td>
                      </tr>
  
  
              {{-- minggu ke tiga --}}
              @php
                  $masukok3 = 0;
              @endphp
                  @foreach ($masuk as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '3')
                        @php
                          $masukok3 += $item->uangmasuk;
                        @endphp
                          
                      @else
                      @endif
                  @endforeach
  
                      @php
                      $keluarok3 = 0;
                      @endphp
                      @foreach ($keluar as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '3')
                        @php
                          $keluarok3 += $item->uangkeluar;
                        @endphp
                          
                      @else
                      @endif
                      @endforeach
                      @php
                          $totalok3 = $masukok3-$keluarok3;
                      @endphp
                      <tr>
                        <td>3</td>
                        <td><p> @currency($masukok3)</p></td>
                        <td><p> @currency($keluarok3)</p></td>
                        <td><p> @currency($totalok3)</p></td>
                      </tr>
  
               {{-- minggu keempat --}}
              @php
                  $masukok4 = 0;
              @endphp
                  @foreach ($masuk as $item)
   
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '4')
                      @php
                      $masukok4 += $item->uangmasuk;
                      @endphp
                   
                      @else
                      @endif
                  @endforeach
  
                      @php
                      $keluarok4 = 0;
                      @endphp
                      @foreach ($keluar as $item)
          
                      @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '4')
                      @php
                          $keluarok4 += $item->uangkeluar;
                      @endphp
                          
                      @else
                      @endif
                      @endforeach
                      @php
                          $totalok4 = $masukok4-$keluarok4;
                      @endphp
                      <tr>
                      <td>4</td>
                      <td><p> @currency($masukok4)</p></td>
                      <td><p> @currency($keluarok4)</p></td>
                      <td><p> @currency($totalok4)</p></td>
                      </tr>
      
             
           
          </tbody>
        </table>
      
      </div>
    </div>

    <hr>
    <center>
        <h3>Rincian Uang Masuk Bulan Ini</h3>
        
    </center>
    <hr>

    <div class="card">
     
      <div class="card-body">
  
  
        <table class="table table-bordered table-striped">
          <thead style="background-color: lightblue">
            <tr>
                <tr>
                    <th scope="col">Minggu Ke-</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Uang Masuk</th>
                    <th scope="col">Rincian</th>
                  </tr>
             
            </tr>
          </thead>
          <tbody>
              {{-- minggu pertama --}}
          @php
          $total = 0;
        @endphp
          @foreach ($masuk as $item)

          @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '1')
          <tr>
             <td>1</td>
            <td>{{ $item->tgl}} </td>
            <td>@currency($item->uangmasuk)</td>
            <td>{{ $item->keterangan }}</td>
            @php
              $total += $item->uangmasuk;
            @endphp
          
          </tr>
              
          @else
          @endif
          @endforeach
          <tr>
            <td colspan="4"><p><b> Total Pemasukan Minggu Ke-1 : @currency($total)</b></p></td>
          </tr>

          {{-- minggu kedua --}}
          @php
            $total2 = 0;
          @endphp
          @foreach ($masuk as $item)

          @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '2')
          <tr>
             <td>2</td>
            <td>{{ $item->tgl}} </td>
            <td>@currency($item->uangmasuk)</td>
            <td>{{ $item->keterangan }}</td>
            @php
            $total2 += $item->uangmasuk;
            @endphp
          </tr>
              
          @else
              
          @endif
             
          @endforeach
          <tr>
            <td colspan="4"><p><b> Total Pemasukan Minggu Ke-2 : @currency($total2)</b></p></td>
          </tr>


          {{-- minggu ketiga --}}

          @php
            $total3 = 0;
          @endphp
          @foreach ($masuk as $item)

          @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '3')
          <tr>
             <td>3</td>
            <td>{{ $item->tgl}} </td>
            <td>@currency($item->uangmasuk)</td>
            <td>{{ $item->keterangan }}</td>
            @php
            $total3 += $item->uangmasuk;
            @endphp
          </tr>
              
          @else
              
          @endif
             
          @endforeach
          <tr>
            <td colspan="4"><p><b> Total Pemasukan Minggu Ke-3 : @currency($total3)</b></p></td>
          </tr>

          {{-- minggu keempat --}}
          @php
            $total4 = 0;
          @endphp
          @foreach ($masuk as $item)

          @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '4')
          <tr>
             <td>4</td>
            <td>{{ $item->tgl}} </td>
            <td>@currency($item->uangmasuk)</td>
            <td>{{ $item->keterangan }}</td>
            @php
            $total4 += $item->uangmasuk;
            @endphp
          </tr>
              
          @else
              
          @endif
             
          @endforeach
          <tr>
            <td colspan="4"><p><b> Total Pemasukan Minggu Ke-4 : @currency($total4)</b></p></td>
          </tr>
       
      </tbody>
    </table>
    
</div>
</div>

<hr>
    <center>
        <h3>Laporan Uang Keluar Bulan Ini</h3>
        
    </center>
    <hr>

<div class="card">
  
  <div class="card-body">


    <table class="table table-bordered table-striped">
      <thead style="background-color: lightblue">
        <tr>
            <tr>
                <th scope="col">Minggu Ke-</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Uang Keluar</th>
                <th scope="col">Rincian</th>
              </tr>
         
        </tr>
      </thead>
      <tbody>
        {{-- minggu pertama --}}
  @php
  $total = 0;
@endphp
  @foreach ($keluar as $item)

  @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '1')
  <tr>
     <td>1</td>
    <td>{{ $item->tgl}} </td>
    <td>@currency($item->uangkeluar)</td>
    <td>{{ $item->keterangan }}</td>
    @php
      $total += $item->uangkeluar;
    @endphp
  
  </tr>
      
  @else
  @endif
  @endforeach
  <tr>
    <td colspan="4"><p><b> Total Pengeluaran Minggu Ke-1 : @currency($total)</b></p></td>
  </tr>

  {{-- minggu kedua --}}
  @php
    $total2 = 0;
  @endphp
  @foreach ($keluar as $item)

  @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '2')
  <tr>
     <td>2</td>
    <td>{{ $item->tgl}} </td>
    <td>@currency($item->uangkeluar)</td>
    <td>{{ $item->keterangan }}</td>
    @php
    $total2 += $item->uangkeluar;
    @endphp
  </tr>
      
  @else
      
  @endif
     
  @endforeach
  <tr>
    <td colspan="4"><p><b> Total Pengeluaran Minggu Ke-2 : @currency($total2)</b></p></td>
  </tr>


  {{-- minggu ketiga --}}

  @php
    $total3 = 0;
  @endphp
  @foreach ($keluar as $item)

  @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '3')
  <tr>
     <td>3</td>
    <td>{{ $item->tgl}} </td>
    <td>@currency($item->uangkeluar)</td>
    <td>{{ $item->keterangan }}</td>
    @php
    $total3 += $item->uangkeluar;
    @endphp
  </tr>
      
  @else
      
  @endif
     
  @endforeach
  <tr>
    <td colspan="4"><p><b> Total Pengeluaran Minggu Ke-3 : @currency($total3)</b></p></td>
  </tr>

  {{-- minggu keempat --}}
  @php
    $total4 = 0;
  @endphp
  @foreach ($keluar as $item)

  @if ($item->bulan === $mytime->format('Y-m') and $item->minggu === '4')
  <tr>
     <td>4</td>
    <td>{{ $item->tgl}} </td>
    <td>@currency($item->uangkeluar)</td>
    <td>{{ $item->keterangan }}</td>
    @php
    $total4 += $item->uangkeluar;
    @endphp
  </tr>
      
  @else
      
  @endif
     
  @endforeach
  <tr>
    <td colspan="4"><p><b> Total Pengeluaran Minggu Ke-4 : @currency($total4)</b></p></td>
  </tr>
   
  </tbody>
</table>

</div>
</div>

 
    
    

@endsection








